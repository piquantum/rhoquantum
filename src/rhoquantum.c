/**
 * @author      : oli (oli@$HOSTNAME)
 * @file        : main
 * @created     : Friday Mar 01, 2024 19:16:33 GMT
 */

#include "rhoquantum.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// #define RQ_DEBUG 0

#define HUE_OFFSET 	0.5f
#define SAT_MIN 	0.3f // for the zero state, this is the amount of
#define SAT_MAX		1.0f
#define LIGHTNESS_MIN 	30
#define LIGHTNESS_AVG 	30
#define LIGHTNESS_MAX	60

#define MAX_RAND_NUMS 0x400
#define MAX_GATE_SIZE 2 /* we have single and two qubit gates atm */

#define REAL 2 /* real values are at even index of vector */
#define CMPLX 1 /* offset in state vector for start of complex numbers*/
#define VCMPLX 1 /* offset in vect2 for start of complex numbers */
#define MCMPLX 1 /* offset in Matrix2 for start of complex numbers */

#define PI 3.14159265359f

// define PRECISION to use 16bit 0.16 fixed point
// undef to use 32bit 16.16 everywhere
#ifdef USE_FLOATS
#define ZERO 0.0f
#define ONE 1.0f
#define INVR2 0.707106781f
#else // no floats
#define ZERO 0x0
#define ONE 		0x7fff
#define HALF 		0x4000
#define INVR2 		0x5a82 // 0.70710678118 * ONE

// default is int32_t 16.16 fixed point
#define S 15
#define toFixed(val) ( (val) * ONE )
#define toFloat(val) ( (float)(val) / (float)ONE )

// rounds to the nearest integer value before right shifting
// https://www.embeddedrelated.com/showarticle/1015.php
#define rshf(val) ( ((val) + HALF) >> S )
#define fpMul(a, b) ( rshf( (int32_t)(a) * (int32_t)(b)) )
#endif // USE_FLOATS

// Gates
#define NUM_REAL_GATES 		3
#define NUM_CMPLX_GATES 	1
#define NUM_VARIABLE_REAL_GATES 1
#define NUM_VARIABLE_CMPLX_GATES 2

// these are just convenience values for picking random gates
#define REAL_GATE_OFFSET 		0
#define CMPLX_GATE_OFFSET 		NUM_REAL_GATES
#define VARIABLE_REAL_GATE_OFFSET	(CMPLX_GATE_OFFSET + NUM_CMPLX_GATES)
#define VARIABLE_CMPLX_GATE_OFFSET 	(VARIABLE_REAL_GATE_OFFSET + NUM_VARIABLE_REAL_GATES)

#define TOTAL_GATES (NUM_REAL_GATES + NUM_CMPLX_GATES + NUM_VARIABLE_REAL_GATES + NUM_VARIABLE_CMPLX_GATES)

uint8_t randGates[MAX_RAND_NUMS], randQubits[MAX_RAND_NUMS];
uint8_t randGateSize[MAX_RAND_NUMS], randCtrlQubits[MAX_RAND_NUMS];
vtype randAngles[MAX_RAND_NUMS];

// TODO
// BUILD SYSTEM on MCU
//
// Controller support, MCU has 2 usb ports that can use FULL SPEED
// 	use usb stack from M32
//
// Neopixel library for LED out
//
// Screen input / output and how to use it?
//
// Audio output?
//
// Circuit serial input/output -> Just writing to file
// 	for loading quantum circuits
// 	pyserial library

uint32_t SEED = 0xaced;
Matrix2 X = {ZERO,ZERO, ONE,ZERO, ONE,ZERO, ZERO,ZERO};
Matrix2 Z = {ONE,ZERO, ZERO,ZERO, ZERO,ZERO, -ONE,ZERO};
Matrix2 H = {INVR2,ZERO, INVR2,ZERO, INVR2,ZERO, -INVR2,ZERO};
Matrix2 Y = {ZERO,ZERO, ZERO,-ONE, ZERO,ONE, ZERO,ZERO};
Matrix2 T = {ONE,ZERO, ZERO,ZERO, ZERO,ZERO, INVR2,INVR2};
Matrix2 Rx, Ry, Rz;

void setVacuum(State *s)
{
	memset(s->v, 0x0, MAX_STATE_VECT_SIZE * sizeof(*s->v));
	s->v[0] = ONE;
}

// see defc.h for where these are declared :3
// This needs to be called at least ONCE at the start of the program
State *makeState(void)
{
	int32_t i;
	printf("sizeof(vtype) * 8: %lu bits, State * size: %lu MB\n",
			sizeof(vtype) * 8,
			MAX_STATE_VECT_SIZE * sizeof(vtype) / (1024*1024));
	printf("NUM_QUBITS: %d, VECTOR SIZE: %d, VSIZE: %d\n",
			NUM_QUBITS, MAX_STATE_VECT_SIZE,  VSIZE);
	printf("REAL: %d, CMPLX: %d, VCMPLX: %d, MCMPLX: %d\n",
			REAL, CMPLX, VCMPLX, MCMPLX);
	printf("MAX_RAND_NUMS: %d, MAX_GATE_SIZE: %d\n",
			MAX_RAND_NUMS, MAX_GATE_SIZE);
	printf("NUM GATES: %d\n", TOTAL_GATES);
	printf("\tNUM_REAL_GATES: %d, NUM_CMPLX_GATES: %d\n",
			NUM_REAL_GATES, NUM_CMPLX_GATES);
	printf("\tNUM_VARIABLE_REAL_GATES: %d, NUM_VARIABLE_CMPLX_GATES: %d\n",
			NUM_VARIABLE_REAL_GATES, NUM_VARIABLE_CMPLX_GATES);

	State *s;
	s = malloc(sizeof(*s));
	if (s == NULL) {
		printf("ERROR allocating State\n");
		exit(1);
		return NULL;
	}

#ifdef SDRAM
	// s->v = (volatile vtype *)(0xc0000000);
	s->v = (vtype *)(0xc0000000);
#endif
	setVacuum(s);
	s->basisState = 0;

	srand(SEED);

	// setup random
	s->randomGateIdx = 0;
	for(i = 0; i < MAX_RAND_NUMS; i++) {
		// all gates
		randGates[i] = rand_max(TOTAL_GATES);
		randQubits[i] = rand_max(NUM_QUBITS);
		randCtrlQubits[i] = rand_max(NUM_QUBITS);
		// single-qubit gates is 0 or two-qubit gates is 1
		randGateSize[i] = rand_max(MAX_GATE_SIZE+1);
#ifdef USE_FLOATS
		randAngles[i] = randf(); // want -1 to 1
#else
		randAngles[i] = rand_max(2 * ONE) - ONE; // -ONE to ONE
#endif
	}

	// setup instruction history
	memset(s->lastCmdLen, '\0', MAX_SAVED_CMDS);

	// setup colours
	for (i = 0; i < NUM_QUBITS; i++) {
		s->hslColours[i][0] = HUE_OFFSET * 255;
		s->hslColours[i][1] = SAT_MIN * 255;
		// the lightness
		s->hslColours[i][2] = LIGHTNESS_AVG; // half brightness
		hsl2rgb(s->hslColours[i], s->rgbColours[i]);
	}

	return s;
}

// returns float between [-1.0f, 1.0f]
float randf(void) {
	return ((float)rand() / (float)(RAND_MAX)) * 2.0f - 1.0f;
}

int rand_max(int max) {
	// return randf() * (float)max;
	return rand() % max;
}

// if the matrix is complex
// (a + bi) * (c * di) = (a*c - b*d)  + (b*c + a*d)i
// inline
void matmulCmplx(Matrix2 m, State *s, int32_t i, int32_t j)
{
	// [ m0, m1 ]  ( v0 ) = ( m0*v0 + m1*v1 )
	// [ m2, m3 ]  ( v1 )   ( m2*v0 + m3*v1 )
	//
	// where m0 * v0 = (m0re*v0re - m0im*v0im) + (m0im*v0re + m0re*v0im)i

#ifdef USE_FLOATS
	float res[4], viRe, viIm, vjRe, vjIm;
#else
	int32_t res[4], viRe, viIm, vjRe, vjIm;
#endif
	vtype *v;
	v = s->v;

	viRe = v[i*REAL];
	viIm = v[i*REAL+CMPLX];
	vjRe = v[j*REAL];
	vjIm = v[j*REAL+CMPLX];

	// m0 * vi
	res[0] = (m[0] * viRe) - (m[MCMPLX] * viIm);
	res[VCMPLX] = (m[MCMPLX] * viRe) + (m[0] * viIm);
	// m1 * vj*REAL
	res[0] += (m[REAL] * vjRe) - (m[REAL+MCMPLX] * vjIm);
	res[VCMPLX] += (m[REAL+MCMPLX] * vjRe) + (m[REAL] * vjIm);
	// m2 * vi*REAL
	res[REAL] = (m[2*REAL] * viRe) - (m[2*REAL+MCMPLX] * viIm);
	res[REAL+VCMPLX] = (m[2*REAL+MCMPLX] * viRe) + (m[2*REAL] * viIm);
	// m3 * vj*REAL
	res[REAL] += (m[3*REAL] * vjRe) - (m[3*REAL+MCMPLX] * vjIm);
	res[REAL+VCMPLX]+= (m[3*REAL+MCMPLX] * vjRe) + (m[3*REAL] * vjIm);

	// pri*REALntf("i*REAL: %d, j*REAL: %d\n", i*REAL, j*REAL);
#ifdef USE_FLOATS
	v[i*REAL] = (res[0]);
	v[j*REAL] = (res[REAL]);
	v[i*REAL+CMPLX] = (res[VCMPLX]);
	v[j*REAL+CMPLX] = (res[REAL+VCMPLX]);
#else
	v[i*REAL] = rshf(res[0]);
	v[j*REAL] = rshf(res[REAL]);
	v[i*REAL+CMPLX] = rshf(res[VCMPLX]);
	v[j*REAL+CMPLX] = rshf(res[REAL+VCMPLX]);
#endif
}

// sets the global Rx matrix to angle
void setRx(float twoa)
{
	// Rx(2a) = [ cos(a), -i sin(a) ] = [cos(a), -isin(a), -isin(a), cos(a)]
	// 	    [ -i sin(a), cos(a) ]   // row major
	float a;
	a = twoa * PI;
	memset(Rx, 0x0, 8*sizeof(int32_t));
#ifdef USE_FLOATS
	float ca, sa;
	ca = (cosf(a));
	sa = (sinf(a));
#else
	int32_t ca, sa;
	ca = toFixed(cosf(a));
	sa = toFixed(sinf(a));
#endif
	Rx[0] = ca;
	Rx[REAL + MCMPLX] = -sa;
	Rx[2*REAL + MCMPLX] = -sa;
	Rx[3*REAL] = ca;
}

// sets the global Ry matrix to angle
void setRy(float twoa)
{
	// Ry(2a) = [ cos(a), -sin(a) ] = [ cos(a), -sin(a), sin(a), cos(a) ]
	// 	    [ sin(a),  cos(a) ]   // row major
	float a;
	a = twoa * PI;
	memset(Ry, 0x0, 8*sizeof(int32_t));
#ifdef USE_FLOATS
	float ca, sa;
	ca = (cosf(a));
	sa = (sinf(a));
#else
	int32_t ca, sa;
	ca = toFixed(cosf(a));
	sa = toFixed(sinf(a));
#endif
	Ry[0] = ca;
	Ry[REAL] = -sa;
	Ry[2*REAL] = sa;
	Ry[3*REAL] = ca;
}

// sets the global Rz matrix to angle
void setRz(float twoa)
{
	// Rz(2a) = [ e^(-ia),   0   ]  = [e^(-ia), 0, 0, e^(ia) ]
	//          [   0   , e^(ia) ]    // row major
	float a;
	a = twoa * PI;
	memset(Rz, 0x0, 8*sizeof(int32_t));
#ifdef USE_FLOATS
	float ca, sa;
	ca = (cosf(a));
	sa = (sinf(a));
#else
	int32_t ca, sa;
	ca = toFixed(cosf(a));
	sa = toFixed(sinf(a));
#endif
	// e^(ia) = cos(a) + isin(a)
	// e^(-ia) = cos(a) - isin(a)
	Rz[0] = ca;
	Rz[MCMPLX] = -sa;
	Rz[3*REAL] = ca;
	Rz[3*REAL+MCMPLX] = sa;
}


// function for sparse multiplication of a single-qubit matrix and the state
// vector.
// State vector V 2^numQubits, 2x2 matrix for a single qubit orthogonal matrix
// idx is the qubit index i.e. [0, numQubits-1]
// State  Qubit 0  |    Qubit 1 if we permute the vector basis to this we can
// State *or          |                make the matrix look block diagonal
// 00  [ a b . . ] | [ a . b . ]    00 [ a b . . ]
// 01  [ c d . . ] | [ . e . f ] -> 10 [ c d . . ]
// 10  [ . . e f ] | [ c . d . ]    01 [ . . e f ]
// 11  [ . . g h ] | [ . g . h ]    11 [ . . g h ]
//
// where 'o' is Kronecker product
// I_2 o I_2 o U_2 o I_2 o I_2
void singleQubit(State *s, Matrix2 m, uint8_t idx)
{
	int32_t offset0, offset1, i, j;
	int32_t idx0, idx1;

	offset0 = 1 << idx;		// index of |0> for qubit idx
	offset1 = 1 << (idx + 1);	// index of |1> for qubit idx
	for(i = 0; i < VSIZE; i += offset1) {	// loop over |1>
		for(j = 0; j < offset0; j++) {	// loop over |0>
			idx0 = i + j;
			idx1 = idx0 + offset0;
			matmulCmplx(m, s, idx0, idx1);
		}
	}
}

// [ I 0 ]
// [ 0 U ]
void twoQubit(State *s, Matrix2 m, uint8_t ctrl, uint8_t targ)
{
	int32_t offset0, offset1, i, j, ctrlbit, idx0, idx1;

	offset0 = 1 << targ;
	offset1 = 1 << (targ + 1);
	ctrlbit = 1 << ctrl;

	for(i = 0; i < VSIZE; i += offset1) {
		for(j = 0; j < offset0; j++) {
			idx0 = i + j;
			idx1 = idx0 + offset0;
			if( (idx0 & ctrlbit) && (idx1 & ctrlbit) ) {
				matmulCmplx(m, s, idx0, idx1);
			}
		}
	}
}

void allHadamards(State *s)
{
	int32_t i;
	for(i = 0; i < NUM_QUBITS; i++) {
		singleQubitUpdate(s, H, i);
	}
}

void setAllPlusState(State *s)
{
	setVacuum(s);
	allHadamards(s);
}

// picks a random number, r \in [0,1]
//  then iterates through the state calculating the cumulative probability,
//  once the current cumulative prob >= r, return that element of the state
//  vector. I.e. the qubits state is the index of the state vector in binary.
// should this be fixed precision?
int32_t measure(State *s)
{
	float r, p, vr, vi;
	uint32_t i;

	r = randf();
	r = r*r;
	p = 0.0f;
loop:
	for (i = 0; i < VSIZE; i++) {
#ifdef USE_FLOATS
		vr = (s->v[i*REAL]);
		vi = (s->v[i*REAL+CMPLX]);
#else
		vr = toFloat(s->v[i*REAL]);
		vi = toFloat(s->v[i*REAL+CMPLX]);
#endif

		vr = vr*vr;
		vi = vi*vi;
		p += vr + vi;
		// printf("\t M: r=%f, p=%f\n", r, p);
		// TODO
		if (p >= r) { // this will be problematic if we get to
			      //  subnormal states. As r could be > than the
			      //  max cumulative probability of the state.
			return i;
		}
	}
	// if we got through the list without picking one, rescale and go
	// again ;)
	r *= p;
	goto loop; // solved it
}

int32_t nextBasisState(State *s)
{
	float vr, vi, epsilon;
	int32_t res;
	epsilon = 1e-5;
	for (; s->basisState < VSIZE; s->basisState++) {
#ifdef USE_FLOATS
		vr = (s->v[s->basisState*REAL]);
		vi = (s->v[s->basisState*REAL+CMPLX]);
#else
		vr = toFloat(s->v[s->basisState*REAL]);
		vi = toFloat(s->v[s->basisState*REAL+CMPLX]);
#endif
		vr = vr * vr;
		vi = vi * vi;
		if ( (vr + vi) >= epsilon) {
			res = s->basisState;
			s->basisState++;
			return res;
		}
	}
	// if we didn't get one go back to zero
	s->basisState = 0;
	return -1;
}

void printBin(int32_t val)
{
	int32_t i;
	for (i = NUM_QUBITS - 1; i >= 0; i--) {
		if ( ((i+1) % 4) == 0 ) {printf("_"); }
		if (val & (1<<i)) { 	printf("1"); }
		else { 			printf("0"); }
	}
}

void printv(State *s)
{
	int32_t i, idx;
	float epsilon, vrf, vif;
#ifdef USE_FLOATS
	float vr, vi;
#else
	int32_t vr, vi;
#endif
	vtype *v;
	v = s->v;
	epsilon = 0.0001f;
	printf("State vector:\n");
	for(i = 0; i < VSIZE; i++) {
		idx = i * REAL;
		vr = v[idx];	// real
		vi = v[idx+CMPLX];// imaginary
#ifdef USE_FLOATS
		vrf = (vr);
		vif = (vi);
#else
		vrf = toFloat(vr);
		vif = toFloat(vi);
#endif

		if ((vrf*vrf + vif*vif) > epsilon) {
			printf("0x%05x: ", i);
			// printf("(%6d)", vr);
			printf("(% .4f", vrf);
			// printf(", (%6d) ", vi);
			printf(", % .4f)", vif);
			printf(" \t");
			printBin(i);
			printf("\n");
		}
	}
}

void printm(Matrix2 m)
{
	int32_t i, j;
#ifdef USE_FLOATS
	float mr, mi;
#else
	int32_t mr, mi;
#endif
	printf("Matrix2:\n");
	for(i = 0; i < 2 * REAL; i+= REAL) {
		printf("[ ");
		for(j = 0; j < 2 * REAL; j+= REAL) {
			mr = m[i*2+j];	// real
			mi = m[i*2+j+MCMPLX];// imaginary
			printf("(");
			// printf("%04x", mr);
			// printf("%6d", mr);
#ifdef USE_FLOATS
			printf("% 8f", (mr));
			printf(", % 8f", (mi));
#else
			printf("% 8f", toFloat(mr));
			printf(", % 8f", toFloat(mi));
#endif
			printf(")");

			if(j < REAL) {
				printf(", ");
			}
		}
		printf(" ]\n");
	}
}

void updatemAndp(State *s, uint8_t idx, uint8_t idx0, uint8_t idx1)
{
#ifdef USE_FLOATS_FOR_COLOURS
	float v0re, v0im, v1re, v1im;
	float m0, m1, p0, p1;
#else
	int32_t v0re, v0im, v1re, v1im;
	int32_t m0, m1, p0, p1;
#endif

#ifdef USE_FLOATS_FOR_COLOURS
#ifdef USE_FLOATS
			v0re = (s->v[idx0*REAL]);
			v0im = (s->v[idx0*REAL+CMPLX]);
			v1re = (s->v[idx1*REAL]);
			v1im = (s->v[idx1*REAL+CMPLX]);
#else // used vtype
			v0re = toFloat(s->v[idx0*REAL]);
			v0im = toFloat(s->v[idx0*REAL+CMPLX]);
			v1re = toFloat(s->v[idx1*REAL]);
			v1im = toFloat(s->v[idx1*REAL+CMPLX]);
#endif // end USE_FLOATS
			m0 = v0re*v0re + v0im*v0im;
			m1 = v1re*v1re + v1im*v1im;
			s->m[idx][0] += m0;
			s->m[idx][1] += m1;
			p0 = v0re*v1re + v0im*v1im;
			p1 = v0re*v1im - v0im*v1re;
			s->p[idx][0] += p0;
			s->p[idx][1] += p1;
#else
			v0re = (uint32_t)(s->v[idx0*REAL]);
			v0im = (uint32_t)(s->v[idx0*REAL+CMPLX]);
			v1re = (uint32_t)(s->v[idx1*REAL]);
			v1im = (uint32_t)(s->v[idx1*REAL+CMPLX]);
			m0 = rshf(v0re*v0re + v0im*v0im);
			m1 = rshf(v1re*v1re + v1im*v1im);
			s->m[idx][0] += m0;
			s->m[idx][1] += m1;
			p0 = rshf(v0re*v1re + v0im*v1im);
			p1 = rshf(v0re*v1im - v0im*v1re);
			s->p[idx][0] += p0;
			s->p[idx][1] += p1;
#endif

}

void calcAndSetColours(State *s, uint8_t idx)
{
	// colours
	uint8_t hueCenter, hue;
	float saturation;
	float satScale;
	hueCenter = 0;
	satScale = 1.0;

	// now do arg of p
	// hue is between [0,1], the Phase is an angle between
	// [-Pi,Pi] so we +2pi then normalise between 0,1
#ifdef USE_FLOATS_FOR_COLOURS
	s->phi[idx] = (atan2f(s->p[idx][1], s->p[idx][0]) + PI) / (2.0f * PI);
	saturation = s->m[idx][1];
#else
	s->phi[idx] = (atan2f(toFloat(s->p[idx][1]), toFloat(s->p[idx][0])) + PI) / (2.0f * PI);
	saturation = toFloat(s->m[idx][1]);
#endif
	hue = (s->phi[idx] * 255) + (HUE_OFFSET * 255);
	s->hslColours[idx][0] = hue + hueCenter;
	// the lightness
	s->hslColours[idx][2] = LIGHTNESS_AVG; // half brightness
	// the saturation
	// saturate the saturation. LOL
	saturation = ((saturation * satScale) + SAT_MIN);
	if (saturation > 1.0f ) {
		saturation = 1.0f;
		// make capped saturation lighter
		s->hslColours[idx][2] = LIGHTNESS_MAX; 
	}
	s->hslColours[idx][1] = saturation * 255;

	hsl2rgb(s->hslColours[idx], s->rgbColours[idx]);
}

// This applies a single qubit gate and updates the colours to that qubit
void singleQubitUpdate(State *s, Matrix2 m, uint8_t idx)
{
	int32_t offset0, offset1, i, j;
	int32_t idx0, idx1;

	offset0 = 1 << idx;		// index of |0> for qubit idx
	offset1 = 1 << (idx + 1);	// index of |1> for qubit idx

	s->m[idx][0] = ZERO;
	s->m[idx][1] = ZERO;
	s->p[idx][0] = ZERO;
	s->p[idx][1] = ZERO;

	for(i = 0; i < VSIZE; i += offset1) {	// loop over |1>
		for(j = 0; j < offset0; j++) {	// loop over |0>
			idx0 = i + j;
			idx1 = idx0 + offset0;

			// matmulCmplx(m, s, idx0, idx1);
			// updatemAndp(s, idx, idx0, idx1);

			// State vector update
#ifdef USE_FLOATS
			float res[4], v0Re, v0Im, v1Re, v1Im;
#else
			int32_t res[4], v0Re, v0Im, v1Re, v1Im;
#endif
			vtype *v;
			v = s->v;

			v0Re = v[idx0*REAL];
			v0Im = v[idx0*REAL+CMPLX];
			v1Re = v[idx1*REAL];
			v1Im = v[idx1*REAL+CMPLX];

			// m0 * v0
			res[0] = (m[0] * v0Re) - (m[MCMPLX] * v0Im);
			res[VCMPLX] = (m[MCMPLX] * v0Re) + (m[0] * v0Im);
			// m1 * v1*REAL
			res[0] += (m[REAL] * v1Re) - (m[REAL+MCMPLX] * v1Im);
			res[VCMPLX] += (m[REAL+MCMPLX] * v1Re) + (m[REAL] * v1Im);
			// m2 * v0*REAL
			res[REAL] = (m[2*REAL] * v0Re) - (m[2*REAL+MCMPLX] * v0Im);
			res[REAL+VCMPLX] = (m[2*REAL+MCMPLX] * v0Re) + (m[2*REAL] * v0Im);
			// m3 * v1*REAL
			res[REAL] += (m[3*REAL] * v1Re) - (m[3*REAL+MCMPLX] * v1Im);
			res[REAL+VCMPLX]+= (m[3*REAL+MCMPLX] * v1Re) + (m[3*REAL] * v1Im);

			// pri*REALntf("i*REAL: %d, j*REAL: %d\n", i*REAL, j*REAL);
#ifdef USE_FLOATS
			v0Re = res[0];
			v1Re = res[REAL];
			v0Im = res[VCMPLX];
			v1Im = res[REAL+VCMPLX];
#else
			v0Re = rshf(res[0]);
			v1Re = rshf(res[REAL]);
			v0Im = rshf(res[VCMPLX]);
			v1Im = rshf(res[REAL+VCMPLX]);
#endif
			// write back new state vector elements
			v[idx0*REAL] =       v0Re;
			v[idx0*REAL+CMPLX] = v0Im;
			v[idx1*REAL] = 	     v1Re;
			v[idx1*REAL+CMPLX] = v1Im;

			// Calculating partial density matrix
			// write back new density matrix elements
#ifdef USE_FLOATS
			s->m[idx][0] += v0Re*v0Re + v0Im*v0Im;
			s->m[idx][1] += v1Re*v1Re + v1Im*v1Im;
			s->p[idx][0] += v0Re*v1Re + v0Im*v1Im;
			s->p[idx][1] += v0Re*v1Im - v0Im*v1Re;
#else
			s->m[idx][0] += rshf(v0Re*v0Re + v0Im*v0Im);
			s->m[idx][1] += rshf(v1Re*v1Re + v1Im*v1Im);
			s->p[idx][0] += rshf(v0Re*v1Re + v0Im*v1Im);
			s->p[idx][1] += rshf(v0Re*v1Im - v0Im*v1Re);
#endif
		}
	}
	calcAndSetColours(s, idx);
}

// [ I 0 ]
// [ 0 U ]
void twoQubitUpdate(State *s, Matrix2 m, uint8_t ctrl, uint8_t targ)
{
	int32_t offset0, offset1, i, j, ctrlbit, idx0, idx1;

	offset0 = 1 << targ;
	offset1 = 1 << (targ + 1);
	ctrlbit = 1 << ctrl;

	s->m[targ][0] = ZERO;
	s->m[targ][1] = ZERO;
	s->p[targ][0] = ZERO;
	s->p[targ][1] = ZERO;

	for(i = 0; i < VSIZE; i += offset1) {
		for(j = 0; j < offset0; j++) {
			idx0 = i + j;
			idx1 = idx0 + offset0;
			if( !((idx0 & ctrlbit) && (idx1 & ctrlbit)) ) {
				continue;
			}
			// matmulCmplx(m, s, idx0, idx1);
			// updatemAndp(s, targ, idx0, idx1);

			// State vector update
#ifdef USE_FLOATS
			float res[4], v0Re, v0Im, v1Re, v1Im;
#else
			int32_t res[4], v0Re, v0Im, v1Re, v1Im;
#endif
			vtype *v;
			v = s->v;

			v0Re = v[idx0*REAL];
			v0Im = v[idx0*REAL+CMPLX];
			v1Re = v[idx1*REAL];
			v1Im = v[idx1*REAL+CMPLX];

			// m0 * v0
			res[0] = (m[0] * v0Re) - (m[MCMPLX] * v0Im);
			res[VCMPLX] = (m[MCMPLX] * v0Re) + (m[0] * v0Im);
			// m1 * v1*REAL
			res[0] += (m[REAL] * v1Re) - (m[REAL+MCMPLX] * v1Im);
			res[VCMPLX] += (m[REAL+MCMPLX] * v1Re) + (m[REAL] * v1Im);
			// m2 * v0*REAL
			res[REAL] = (m[2*REAL] * v0Re) - (m[2*REAL+MCMPLX] * v0Im);
			res[REAL+VCMPLX] = (m[2*REAL+MCMPLX] * v0Re) + (m[2*REAL] * v0Im);
			// m3 * v1*REAL
			res[REAL] += (m[3*REAL] * v1Re) - (m[3*REAL+MCMPLX] * v1Im);
			res[REAL+VCMPLX]+= (m[3*REAL+MCMPLX] * v1Re) + (m[3*REAL] * v1Im);

			// pri*REALntf("i*REAL: %d, j*REAL: %d\n", i*REAL, j*REAL);
#ifdef USE_FLOATS
			v0Re = res[0];
			v1Re = res[REAL];
			v0Im = res[VCMPLX];
			v1Im = res[REAL+VCMPLX];
#else
			v0Re = rshf(res[0]);
			v1Re = rshf(res[REAL]);
			v0Im = rshf(res[VCMPLX]);
			v1Im = rshf(res[REAL+VCMPLX]);
#endif
			// write back new state vector elements
			v[idx0*REAL] =       v0Re;
			v[idx0*REAL+CMPLX] = v0Im;
			v[idx1*REAL] = 	     v1Re;
			v[idx1*REAL+CMPLX] = v1Im;

			// Calculating partial density matrix
			// write back new density matrix elements
#ifdef USE_FLOATS
			s->m[targ][0] += v0Re*v0Re + v0Im*v0Im;
			s->m[targ][1] += v1Re*v1Re + v1Im*v1Im;
			s->p[targ][0] += v0Re*v1Re + v0Im*v1Im;
			s->p[targ][1] += v0Re*v1Im - v0Im*v1Re;
#else
			s->m[targ][0] += rshf(v0Re*v0Re + v0Im*v0Im);
			s->m[targ][1] += rshf(v1Re*v1Re + v1Im*v1Im);
			s->p[targ][0] += rshf(v0Re*v1Re + v0Im*v1Im);
			s->p[targ][1] += rshf(v0Re*v1Im - v0Im*v1Re);
#endif

		}
	}
	calcAndSetColours(s, targ);
}


// calculates state amplitudes and colours for leds
// reference implementation uses the python src code as a reference
void calcLedColours(State *s, int8_t qidx)
{
	int32_t i, j, offset0, offset1, idx0, idx1;
#ifdef USE_FLOATS_FOR_COLOURS
	float v0re, v0im, v1re, v1im;
	float m0, m1, p0, p1;
#else
	int32_t v0re, v0im, v1re, v1im;
	int32_t m0, m1, p0, p1;
#endif
	vtype *v;

	// colours
	uint8_t hueCenter, hue;
	float saturation;
	float satScale;
	hueCenter = 0;
	satScale = 1.0;

	v = s->v;

	s->m[qidx][0] = ZERO;
	s->m[qidx][1] = ZERO;
	s->p[qidx][0] = ZERO;
	s->p[qidx][1] = ZERO;

	offset0 = 1 << qidx;		// index of |0> for qubit idx
	offset1 = 1 << (qidx + 1);	// index of |1> for qubit idx
	for(i = 0; i < VSIZE; i += offset1) {
		for(j = 0; j < offset0; j++) {
			idx0 = i + j;
			idx1 = idx0 + offset0;
#ifdef RQ_DEBUG
			printf("idx0: %d, idx1: %d\n", idx0, idx1);
#endif
#ifdef USE_FLOATS_FOR_COLOURS
#ifdef USE_FLOATS
			v0re = (v[idx0*REAL]);
			v0im = (v[idx0*REAL+CMPLX]);
			v1re = (v[idx1*REAL]);
			v1im = (v[idx1*REAL+CMPLX]);
#else // used vtype
			v0re = toFloat(v[idx0*REAL]);
			v0im = toFloat(v[idx0*REAL+CMPLX]);
			v1re = toFloat(v[idx1*REAL]);
			v1im = toFloat(v[idx1*REAL+CMPLX]);
#endif // end USE_FLOATS
			m0 = v0re*v0re + v0im*v0im;
			m1 = v1re*v1re + v1im*v1im;
			s->m[qidx][0] += m0;
			s->m[qidx][1] += m1;
			p0 = v0re*v1re + v0im*v1im;
			p1 = v0re*v1im - v0im*v1re;
			s->p[qidx][0] += p0;
			s->p[qidx][1] += p1;
#else
			v0re = (uint32_t)(v[idx0*REAL]);
			v0im = (uint32_t)(v[idx0*REAL+CMPLX]);
			v1re = (uint32_t)(v[idx1*REAL]);
			v1im = (uint32_t)(v[idx1*REAL+CMPLX]);
			m0 = rshf(v0re*v0re + v0im*v0im);
			m1 = rshf(v1re*v1re + v1im*v1im);
			s->m[qidx][0] += m0;
			s->m[qidx][1] += m1;
			p0 = rshf(v0re*v1re + v0im*v1im);
			p1 = rshf(v0re*v1im - v0im*v1re);
			s->p[qidx][0] += p0;
			s->p[qidx][1] += p1;
#endif
		}
	}
	// now do arg of p
	// hue is between [0,1], the Phase is an angle between
	// [-Pi,Pi] so we +2pi then normalise between 0,1
#ifdef USE_FLOATS_FOR_COLOURS
	s->phi[qidx] = (atan2f(s->p[qidx][1], s->p[qidx][0]) + PI) /
								(2.0f * PI);
	saturation = s->m[qidx][1];

	// printf("\tm0: %f, m1: %f, p0: %f, p1: %f\n", (s->m[qidx][0]), (s->m[qidx][1]), (s->p[qidx][0]), (s->p[qidx][1]));
#else
	s->phi[qidx] = (atan2f(toFloat(s->p[qidx][1]), toFloat(s->p[qidx][0])) + PI) /
								(2.0f * PI);
	saturation = toFloat(s->m[qidx][1]);
	// printf("\tm0: %f, m1: %f, p0: %f, p1: %f\n", toFloat(s->m[qidx][0]), toFloat(s->m[qidx][1]), toFloat(s->p[qidx][0]), toFloat(s->p[qidx][1]));
#endif
	// printf("qubit %d Phi = %f\n", qidx, s->phi[qidx]);


	hue = (s->phi[qidx] * 255) + (HUE_OFFSET * 255);
	s->hslColours[qidx][0] = hue + hueCenter;
	// the lightness
	s->hslColours[qidx][2] = 128; // half brightness
	// the saturation
	// saturate the saturation. LOL
	saturation = ((saturation * satScale) + SAT_MIN);
	if (saturation > 1.0f ) {
		saturation = 1.0f;
		s->hslColours[qidx][2] = 210; // make capped saturation lighter
	}
	s->hslColours[qidx][1] = saturation * 255;

	hsl2rgb(s->hslColours[qidx], s->rgbColours[qidx]);
}

// colours
void trueColour(uint8_t *rgb)
{
	printf("\33[38;2;%d;%d;%dm", rgb[0], rgb[1], rgb[2]);
}

// takes pointer to hsl and generates rgb map
void trueHSLColour(uint8_t *hsl)
{
	uint8_t rgb[3];
	hsl2rgb(hsl, rgb);
	trueColour(rgb);
}

// takes a hue in range [0.0f, 1.0f] and sets the terminal colour to that hue
// with saturation 255, lightness = 128;
void trueHue(float hue)
{
	uint8_t hsl[3];
	hsl[0] = hue * 255;
	hsl[1] = 255;
	hsl[2] = 128;
	hsl2rgb(hsl, hsl);
	trueColour(hsl);
}

// TODO check this, taken from
// https://stackoverflow.com/a/9493060
float hue2rgb(float p, float q, float t)
{
	const float oneSixth = 0.16666666666666f;
	const float twoThirds = 0.666666666666f;

	if (t < 0) { t += 1.0f; }
	else if (t > 1) { t -= 1.0f; }

	if (t < oneSixth) { // 1/6 fyi
		return p + (q - p) * 6.0f * t;
	} else if (t < 0.5f) {
		return q;
	} else if ( t < twoThirds) { // 2/3 fyi
		return p + (q - p) * (twoThirds - t) * 6.0f;
	}
	return p;
}

void hsl2rgb(uint8_t *hsl, uint8_t *rgb)
{
	// h, s, l in range [0.0f, 1.0f]
	float h, s, l;
	h = ((float)hsl[0] / 255.0f);
	s = (float)hsl[1] / 255.0f;
	l = (float)hsl[2] / 255.0f;
	// printf("In h:%f, s:%f, l:%f\n", h, s, l);
	// r, g, b in range [0,255]
	float p, q, oneThird;
	oneThird = 0.3333333333f;
	if (s == 0) {
		rgb[0] = 255;
		rgb[1] = 255;
		rgb[2] = 255;
		return;
	}
	q = (l < 0.5f) ? l * (1.0f+s) : l+s -l*s;
	p = 2.0f * l - q;

	rgb[0] = hue2rgb(p, q, h + oneThird) * 255;
	rgb[1] = hue2rgb(p, q, h) * 255;
	rgb[2] = hue2rgb(p, q, h - oneThird) * 255;
}

void printPhaseColourWheel()
{
	// cyn
	// blu
	// purp
	// pink
	// red
	// orange
	// lime
	// green

	uint8_t i;
	float hue, step;
	step = 0.125f;
	hue = 0.5f + HUE_OFFSET;
	for (i = 0; i < 8; i++) {
		trueHue(hue + i*step);
		printf("#");
		printf(CRESET);
	}
	printf("\n");
}

void printQubitAmplitudes(State *s)
{
	int8_t i;
	for (i = 0; i < NUM_QUBITS; i++) {
		calcLedColours(s, i);
#ifdef USE_FLOATS_FOR_COLOURS
		printf("Qubit: %2d, %.2f |0> + %.2f |1>,", i, s->m[i][0], s->m[i][1]);
#else
		printf("Qubit: %2d, %.2f |0> + %.2f |1>,", i, toFloat(s->m[i][0]), toFloat(s->m[i][1]));
#endif
		trueHSLColour(s->hslColours[i]);
		// trueHue(s->phi[i] + HUE_OFFSET);
		printf(" phase angle %.3f\n", s->phi[i]);
		printf(CRESET);
	}
}

void printQubit(State *s)
{
	int8_t i;
	for (i = 0; i < NUM_QUBITS; i++) {
		calcLedColours(s, i);
	}

	// print in hexagon
	//     *   *   *
	//   *   *   *   *
	// *   *   *   *   *
	//   *   *   *   *
	//     *   *   *
	for (i = 0; i < NUM_QUBITS; i++) {
		if ( i == 0 || i == 16 ) { printf("\n\n       "); }
		if ( i == 3 || i == 12 ) { printf("\n\n    "); }
		if ( i == 7 ) { printf("\n\n "); }

		trueColour(s->rgbColours[i]);
		if (i <= 9) { printf("0"); }
		printf("%d    "CRESET, i);
	}
	printf("\n");
}

void printPartialDensityMatrix(State *s)
{
	uint8_t i, lineLen, j;
	lineLen = 3;
	printf("Partial density Matrices:\n");
	for (i = 0; i < NUM_QUBITS; i++) {
		// trueHSLColour((uint8_t[3]){0, 1, 100});
		trueHSLColour(s->hslColours[i]);
		printf("Qubit: %2d", i);
#ifdef USE_FLOATS_FOR_COLOURS
		printf(" [% .3f, % .3f]", s->m[i][0], s->p[i][0]);
#else
		printf(" [% .3f, % .3f]", toFloat(s->m[i][0]), toFloat(s->p[i][0]));
#endif

		printf(CRESET); // reset the colours
		if ((i+1) % lineLen == 0) {
			printf("\n");
			for (j = (i+1)-lineLen; j <= i; j++) {
				trueHSLColour(s->hslColours[j]);
#ifdef USE_FLOATS_FOR_COLOURS
				printf("          [% .3f, % .3f]", s->p[j][1], s->m[j][1]);
#else
				printf("          [% .3f, % .3f]", toFloat(s->p[j][1]), toFloat(s->m[j][1]));
#endif
				if (j != i) {
					printf("    ");
				}
			}
			printf(CRESET); // reset the colours
			printf("\n");
		} else {
			printf("    ");

		}
	}
	// this is a work around as the last one is not a division of 3
	trueHSLColour(s->hslColours[18]);
#ifdef USE_FLOATS_FOR_COLOURS
	printf("\n          [% .3f, % .3f]    ", s->p[18][1], s->m[18][1]);
#else
	printf("\n          [% .3f, % .3f]    ", toFloat(s->p[18][1]), toFloat(s->m[18][1]));
#endif
	printf(CRESET); // reset the colours
	printf("\n");
}

void splitInputOnSpaces(const char buf[INPUT_BUF_SIZE],
			char output[MAX_NUM_ARGS][MAX_ARG_SIZE])
{
	uint8_t tokLen, outputLoc;
	tokLen = 0;
	outputLoc = 0;

	// printf("Buf after null char: %s\n", buf);

	memset(output, '\0', MAX_NUM_ARGS*MAX_ARG_SIZE);
	// split buf on spaces
	char cpy[INPUT_BUF_SIZE];
	strcpy(cpy, buf);
	// printf("Input buf copy is: %s\n", cpy);
	char *tok = strtok(cpy, " ");
	while (tok != NULL) {
		// printf("tok: %s\n", tok);
		tokLen = strlen(tok);
		// clear the output
		// memset(output[outputLoc], '\0', 8);
		strncpy(output[outputLoc++], tok, tokLen);
		tok = strtok(NULL, " ");
	}
}

void saveAlgorithm(State *s, const char *filename)
{
	int8_t i, j;
	FILE *fp;

	fp = fopen(filename, "w");
	if (fp == NULL) {
		printf("Error cannot open file: %s for saving\n", filename);
		return;
	}

	for (i = MAX_SAVED_CMDS-1; i >= 0; i--) {
		if (s->lastCmdLen[i] > 0) {
			// printf("%d) ", i+1);
			for (j = 0; j < s->lastCmdLen[i]; j++) {
				fprintf(fp, "%s ", s->cmdHist[i][j]);
			}
			fprintf(fp, "\n");
		}
	}
	fclose(fp);
}

void loadAlgorithm(State *s, const char *filename)
{
	uint8_t i;
	char buf[INPUT_BUF_SIZE];
	FILE *fp;
	fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("Error file at: %s not found!\n", filename);
		return;
	}

	// read user input into buf
	memset(buf, '\0', INPUT_BUF_SIZE);
	while (fgets(buf, INPUT_BUF_SIZE, fp)) {
		// printf("Buf: %s\n", buf);
		// replace any newline chars with null terminator
		for (i = 0; i < INPUT_BUF_SIZE; i++) {
			if (buf[i] == '\n' || buf[i] == '\0') {
				buf[i] = '\0';
				break;
			}
		}
		// parse the line
		parseArgs(s, buf);
	}
	fclose(fp);
}

void keyboardInput(char buf[INPUT_BUF_SIZE])
{
	// Single qubit:
	// 	gate angle qubit
	// controlled:
	// 	c-gate angle ctrl targ
	// X 0 			// X on qubit 0
	// Rz 0.5 3		// Rz(Pi/2) on qubit 3
	// c-x 0 1		// X controlled on qubit 0 acting on qubit 1
	// c-Rx Pi/4 3 4	// Rx(Pi/4) controlled qubit 3 acting on qubit 4
	uint8_t i;

	// read user input into buf
	memset(buf, '\0', INPUT_BUF_SIZE);

	// help or quit
	printf("Enter a command (h for help): ");
	// Read at most `n` characters (newline included) into `str`.
	// If present, the newline is removed (replaced by the null terminator).
	char* str_read = fgets(buf, INPUT_BUF_SIZE, stdin);
	if (!str_read) {
		return;
	}
	// replace any newline chars with null terminator
	for (i = 0; i < INPUT_BUF_SIZE; i++) {
		if (buf[i] == '\n' || buf[i] == '\0') {
			buf[i] = '\0';
			break;
		}
	}
	// printf("buf: %s\n", buf);
}

void printHelpMessage(void)
{
	printf("\th \tfor help");
	printf("\t\t\tq \tto quit\n");

	printf("\tm \tto measure");
	printf("\t\t\tb \tshow basis states\n");

	printf("\tr \tto reset");
	printf("\t\t\trand \tfor random state\n");

	printf("\tls \tfor history");
	printf("\t\t\ta \tfor qubit amplitudes & phases\n");

	printf("\td \tto print density matrices");
	printf("\tp \tto print state vector\n");

	printf("\t. \tto repeat last command");
	printf("\t\t.n \tto repeat previous <n>th command\n");

	printf("\ts file \tto save gates to file");
	printf("\t\tl file \tto load algorithm from file\n");

	printf("\ts \tfor colour stats\n");

	printf("Single qubit gates, angles in terms of Pi so 0.25 is Pi/4:\n\tX, Y, Z, H, T, Rx, Ry, Rz\n");
	printf("Two qubit gates, just add c- to the start of single qubit gates for controlled gates:\n");
	printf("\tc-x, c-y, c-z, c-t, c-t, c-rx, c-ry, c-rz\n");
	printf("Examples: ");
	printf("You can use lower or upper case for everything\n");
	printf("\tX 0\t\t// X on qubit 0\n");
	printf("\tRz 0.5 3 \t// Rz(Pi/2) on qubit 3\n");
	printf("\tc-x 0 1 \t// X controlled on qubit 0 acting on qubit 1\n");
	printf("\tc-Rx 0.25 3 4	// Rx(Pi/4) controlled qubit 3 acting on qubit 4\n");


}

// At one point I did understand how this function worked.
void parseArgs(State *s, const char buf[INPUT_BUF_SIZE])
{
	// parse the space separated values
	// Single qubit:
	// 	gate angle qubit
	// controlled:
	// 	c-gate angle ctrl targ
	// X 0 			// X on qubit 0
	// Rz Pi/2 3		// Rz(Pi/2) on qubit 3
	// c-x 0 1		// X controlled on qubit 0 acting on qubit 1
	// c-Rx Pi/4 3 4	// Rx(Pi/4) controlled qubit 3 acting on qubit 4
	char c;
	Matrix2 *gate;
	uint8_t qidx, ctrl, strLen, inputLoc, j;
	int8_t i;
	float angle;
	const char * str;
	int32_t measurement, basis;
	// these are static so that you can use '.' to repeat the last command
	// static char lastArg[MAX_SAVED_CMDS][MAX_NUM_ARGS][MAX_ARG_SIZE];
	// static uint8_t lastArgLen[MAX_SAVED_CMDS];
	uint8_t cmdOffset; // holds the number of which previous command to run
			   // these are used for swapping the -2 and -1 cmds when .. is called
	char tmpArg[MAX_NUM_ARGS][MAX_ARG_SIZE];
	uint8_t tmpLen;
	int8_t isGateCommand;
	char input[MAX_NUM_ARGS][MAX_ARG_SIZE];

	// takes a space separated string like:
	// 	x 0
	// 	c-x 2 5
	// 	rz 0.142 6
	// and parses it into the input format which is first index is space
	// separated value, i.e. x, c-x, rz in examples above then second index
	// is the char in that string
	splitInputOnSpaces(buf, input);

	for (inputLoc = 0; inputLoc < MAX_NUM_ARGS; inputLoc++) {
		if (input[inputLoc][0] == '\0') {
			break;
		}
	}

	// not valid
	if (inputLoc < 1) {
		printHelpMessage();
		return;
	}

	if (input[0][0] == 'l' && input[0][1] == 's') { // show history ls
		printf("Previous commands are:\n");
		for (i = MAX_SAVED_CMDS - 1; i >= 0; i--) {
			if (s->lastCmdLen[i] > 0) {
				printf("%3d) ", i+1);
				for (j = 0; j < s->lastCmdLen[i]; j++) {
					printf(" %s ", s->cmdHist[i][j]);
				}
				printf("\n");
			}
		}
		return; // return early so we don't mess up the command history
	}

	// I really hope this isn't undefined behaviour.
	if (input[0][0] == '.') { // repeat the last command [0]
		cmdOffset = 0;
		if ('1' <= input[0][1] && input[0][1] <= '8') {
			cmdOffset = atoi(&input[0][1])-1;
		}
		if (input[0][1] == '.') { // repeat the [1] command
			cmdOffset = 1;
		}

		// need to decide if we push back duplicate commands, probably
		// should then you can just send a list and it reproduces
		// exactly.
		// if (cmdOffset >= 0) {
		// move all the previous args back one slot and insert
		// the repeated arg at the top
		memcpy(&tmpArg[0][0], &s->cmdHist[cmdOffset][0][0], MAX_NUM_ARGS * MAX_ARG_SIZE);
		tmpLen = s->lastCmdLen[cmdOffset];
		for (i = MAX_SAVED_CMDS - 1; i >= 1; i--) {
			memcpy(&s->cmdHist[i][0][0], &s->cmdHist[i-1][0][0], MAX_NUM_ARGS * MAX_ARG_SIZE);
			s->lastCmdLen[i] = s->lastCmdLen[i-1];
		}
		memcpy(&s->cmdHist[0][0][0], &tmpArg[0][0], MAX_NUM_ARGS * MAX_ARG_SIZE);
		s->lastCmdLen[0] = tmpLen;
		// } // cmdoffset
		printf("Repeating: ");
		for (i = 0; i < s->lastCmdLen[0]; i++) {
			printf("%s ", s->cmdHist[0][i]);
		}
		printf("\n");
		// we just copy the previous command to the current input
		memcpy(&input[0][0], &s->cmdHist[0][0][0], MAX_NUM_ARGS * MAX_ARG_SIZE);
		// input = s->cmdHist[0];
		inputLoc = s->lastCmdLen[0];
	} else {
		// check if input is the same as the last argument, if it is
		// skip adding it. Otherwise push all previous back and add
		// input to the first element
		// sameInput = 0;
		// for (i = 0; i < inputLoc; i++) {
		// 	sameInput |= strcmp(input[i], s->cmdHist[0][i]);
		// }
		// if they are not the same
		// if (sameInput != 0) {
		// default is only write 2+ argument commands
		// but we want to also save reset 'r' and NOT save/load cmmds
		isGateCommand = (inputLoc > 1);

		// do record resets
		if (inputLoc == 1 && input[0][0] == 'r') {
			isGateCommand = 1;
		}
		// don't record save or loads
		if (inputLoc == 2 &&
				(input[0][0] == 's' || input[0][0] == 'l')) {
			isGateCommand = 0;
		}
		if (isGateCommand) {
			for (i = MAX_SAVED_CMDS - 1; i >= 1; i--) {
				// copy s->cmdHist[0] to s->cmdHist[1]
				memcpy(&s->cmdHist[i][0][0], &s->cmdHist[i-1][0][0], MAX_NUM_ARGS * MAX_ARG_SIZE);
				s->lastCmdLen[i] = s->lastCmdLen[i-1];
			}
			// then input to s->cmdHist[0]
			memcpy(&s->cmdHist[0][0][0], &input[0][0], MAX_NUM_ARGS * MAX_ARG_SIZE);
			s->lastCmdLen[0] = inputLoc;
		}
	}

	str = input[0];
	strLen = strlen(str);

	if (inputLoc == 1) {
		if (str[0] == 'h') { // help time!
			printHelpMessage();
			return;
		} else if (str[0] == 'q') { // q or quit
			printf("Exiting\n");
			exit(0);
		} else if (str[0] == 'm') { // measure
			measurement = measure(s);
			printf("Measurement: 0x%05x = ", measurement);
			printBin(measurement); printf("\n");
			return;
		} else if (str[0] == 'r') { // reset or random
			if (strLen == 1) { // reset
				setVacuum(s);
				return;
			} else if (str[1] == 'a') { // for rand or random
				printf("Doing random circuit this may take a few seconds...\n");
				doRandomCircuit(s);
				return;
			}
		} else if (str[0] == 'a') { // show qubit amplitudes
			printQubitAmplitudes(s);
			return;
		} else if (str[0] == 's') {  // color stats
			printPhaseColourWheel();
			for (i = 0; i < NUM_QUBITS; i++) {
				trueHSLColour(s->hslColours[i]);
				printf("Hue: %3d, Saturation: %3d, Lightness: %3d\n",
						s->hslColours[i][0], s->hslColours[i][1], s->hslColours[i][2]);
				printf(CRESET);
			}
			return;
		} else if (str[0] == 'b' || (str[0] == 'c' && str[1] == 'y')) {
			// cycle mode, showing elements in the state vector
			while ((basis = nextBasisState(s)) != -1) {
				printf("Basis state: 0x%05x = ", basis);
				printBin(basis); printf("\n");
				// what we do is the states that are 0 we set the
				// saturation to satMin
				// The states that are 1 we set the lightness to
				// lightnessOne
				for (i = 0; i < NUM_QUBITS; i++) {
					// printf("Basis: %d, i: %d, Basis & %d\n",
					// 		basis, i, basis & (1<< i));
					if (basis & (1 << i)) {
						// s->hslColours[i][1] = 255;
						s->hslColours[i][2] = LIGHTNESS_MAX;
					} else {
						// s->hslColours[i][1] = SAT_MIN * 255;
						s->hslColours[i][2] = LIGHTNESS_MIN;
					}
				}
				for (i = 0; i < NUM_QUBITS; i++) {
					// print in hexagon
					//     *   *   *
					//   *   *   *   *
					// *   *   *   *   *
					//   *   *   *   *
					//     *   *   *
					if ( i == 0 || i == 16 ) { printf("\n\n       "); }
					if ( i == 3 || i == 12 ) { printf("\n\n    "); }
					if ( i == 7 ) { printf("\n\n "); }
					trueHSLColour(s->hslColours[i]);
					if (i <= 9) { printf("0"); }
					printf("%d    "CRESET, i);
				}
				printf("\n");
			}
			return;
		} else if (str[0] == 'p') { // print the state vector
			printv(s);
			return;
		} else if(str[0] == 'd') { // density matrices
			printPartialDensityMatrix(s);
			return;
		}
	}

	if (inputLoc == 2) {// single qubit gate i.e. X 0 or Y 4
		if (str[0] == 'l') { // load algorithm
			// add .rq file extension
			char loadFilename[32];
			strcpy(loadFilename, input[1]);
			strcat(loadFilename, ".rq");
			printf("Loading algorithm from: %s\n", loadFilename);
			loadAlgorithm(s, loadFilename);
			return;
		} else if (str[0] == 's') { // save algorithm
			// add .rq file extension
			char saveFilename[32];
			strcpy(saveFilename, input[1]);
			strcat(saveFilename, ".rq");
			printf("Saving to: %s\n", saveFilename);
		        saveAlgorithm(s, saveFilename);
			return;
		}

		if (strLen != 1) {
			printf("Error not a valid input (inputLoc == 2) %s\n", str);
			return;
		}
		// x, y, z, h, t
		c = str[0];
		if (c == 'x' || c == 'X') { gate = &X; }
		else if (c == 'y' || c == 'Y') { gate = &Y; }
		else if (c == 'z' || c == 'Z') { gate = &Z; }
		else if (c == 'h' || c == 'H') { gate = &H; }
		else if (c == 't' || c == 'T') { gate = &T; }
		else {
			printf("Error not a valid gate %c\n", c);
			// exit(1);
			return;
		}
		qidx = atoi(input[1]);
		singleQubitUpdate(s, *gate, qidx);
		return;
	} else if ( inputLoc == 3) { // either rx pi 0 or c-x 4 5
		if (strLen == 2) { // rx, ry, rz single qubit rotation
			c = str[0];
			if (c != 'r' && c != 'R') {
				printf("Error not valid Rotation gate %c\n", c);
				// exit(2);
				return;
			}
			// if rotation gate get angle which is next string.
			c = str[1];
			angle = atof(input[1]);
			if (c == 'x' || c == 'X') {
				setRx(angle);
				gate = &Rx;
			} else if (c == 'y' || c == 'Y') {
				setRy(angle);
				gate = &Ry;
			} else if (c == 'z' || c == 'Z') {
				setRz(angle);
				gate = &Rz;
			} else {
				printf("Error not a valid rotation gate %c\n", c);
				// exit(3);
				return;
			}
			qidx = atoi(input[2]);
			singleQubitUpdate(s, *gate, qidx);
			return;
		} else if (strLen == 3) { // c-x c-y, c-z, c-h, c-t
			c = str[0];
			if ( c != 'c' && c != 'C') {
				printf("Error not valid control gate %c\n", c);
				// exit(3);
				return;
			}
			c = str[2];
			if (c == 'x' || c == 'X') { gate = &X; }
			else if (c == 'y' || c == 'Y') { gate = &Y; }
			else if (c == 'z' || c == 'Z') { gate = &Z; }
			else if (c == 'h' || c == 'H') { gate = &H; }
			else if (c == 't' || c == 'T') { gate = &T; }
			else {
				printf("Error not a valid gate %c\n", c);
				// exit(1);
				return;
			}
			ctrl = atoi(input[1]);
			qidx = atoi(input[2]);
			twoQubitUpdate(s, *gate, ctrl, qidx);
			return;
		} else {
			printf("not a valid input (controlled gate) %s\n", str);
			// exit(1);
			return;
		}
	} else if (inputLoc == 4) { // c-Rx Pi/4 3 4
		if (strLen != 4) {
			printf("Error not a valid input (controlled rotation gate) %s\n", str);
			// exit(1);
			return;
		}
		c = str[0];
		if ( c != 'c' && c != 'C') {
			printf("Error not valid control gate %c\n", c);
			// exit(4);
			return;
		}
		// if rotation gate get angle which is next string.
		c = str[3];
		angle = atof(input[1]);
		if (c == 'x' || c == 'X') { setRx(angle); gate = &Rx; }
		else if (c == 'y' || c == 'Y') { setRy(angle); gate = &Ry; }
		else if (c == 'z' || c == 'Z') { setRz(angle); gate = &Rz; }
		else {
			printf("Error not a valid rotation gate %c\n", c);
			// exit(3);
			return;
		}
		ctrl = atoi(input[2]);
		qidx = atoi(input[3]);
		twoQubit(s, *gate, ctrl, qidx);
		return;
	}

	// if we fall through to the end of the function we got an invalid
	// input
	printf("Error not a valid input: ");
	for (i = 0; i < inputLoc; i++) {
		printf("%s ", input[i]);
	}
}

// writes the random values to disk for checking
void writeRandomNumsToDisk(void)
{
	State *s;
	s = makeState();
	FILE *fgates, *fqubits, *fangles, *fctrl, *fgateSize;
	fgates = fopen("gates.txt", "w");
	fqubits = fopen("qubits.txt", "w");
	fangles = fopen("angles.txt", "w");
	fctrl = fopen("ctrlqubits.txt", "w");
	fgateSize = fopen("gateSize.txt", "w");
	int32_t i;
	for(i = 0; i < MAX_RAND_NUMS; i++) {
		fprintf(fgates, "%d\n", randGates[i]);
		fprintf(fqubits, "%d\n", randQubits[i]);
#ifdef USE_FLOATS
		fprintf(fangles, "%f\n", randAngles[i]);
#else
		fprintf(fangles, "%f\n", toFloat(randAngles[i]));
#endif
		fprintf(fctrl, "%d\n", randCtrlQubits[i]);
		fprintf(fgateSize, "%d\n", randGateSize[i]);
	}
	free(s);
}


// this takes a gate choice r between 0..<TOTAL_NUM_GATES
// a qubit index and angle 'a'
void randomGate(State *s, uint8_t r, uint8_t qidx, vtype a,
		uint8_t ctrlqidx, uint8_t gateSize)
{
	Matrix2 *gate;

#ifdef RQ_DEBUG
	printf("r: %d, qidx: %d\n", r, qidx);
#endif
	char buf[24];
	uint8_t MAX = 24;
	uint8_t len = 0;

	if (gateSize == 2) { len += snprintf(buf, MAX, "C-"); }
	if(r < NUM_REAL_GATES) {
		switch (r) {
			case 0: gate = &X; 
				len += snprintf(buf+len, MAX, "X ");
				break;
			case 1: gate = &H;
				len += snprintf(buf+len, MAX, "H ");
				break;
			case 2: gate = &Z;
				len += snprintf(buf+len, MAX, "Z ");
				break;
		}
	} else if(r < CMPLX_GATE_OFFSET + NUM_CMPLX_GATES) {
		r = r - NUM_REAL_GATES;
		switch (r) { 
			case 0: gate = &Y; 
				len += snprintf(buf+len, MAX, "Y ");
				break;
		}
	} else if(r < VARIABLE_REAL_GATE_OFFSET + NUM_VARIABLE_REAL_GATES) {
		r = r - VARIABLE_REAL_GATE_OFFSET;
#ifdef USE_FLOATS
		setRy(a); gate = &Ry; 
		len += snprintf(buf+len, MAX, "Ry(%1.3fpi) ", a);
#else
		setRy(toFloat(a)); gate = &Ry;
		len += snprintf(buf+len, MAX, "Ry(%1.3fpi) ", toFloat(a));
#endif
	} else if(r < VARIABLE_CMPLX_GATE_OFFSET + NUM_VARIABLE_CMPLX_GATES) {
		r = r - VARIABLE_CMPLX_GATE_OFFSET;
		switch(r) {
#ifdef USE_FLOATS
			case 0: 
				setRx((a)); gate = &Rx; 
				len += snprintf(buf+len, MAX, "Rx(%1.3fpi) ", a); 
				break;
			case 1: 
				setRz((a)); gate = &Rz; 
				len += snprintf(buf+len, MAX, "Rz(%1.3fpi) ", a);
				break;
#else
			case 0: 
				setRx(toFloat(a)); gate = &Rx; 
				len += snprintf(buf+len, MAX, "Rx(%1.3fpi) ", toFloat(a));
				break;
			case 1: 
				setRz(toFloat(a)); gate = &Rz; 
				len += snprintf(buf+len, MAX, "Rz(%1.3fpi) ", toFloat(a));
				break;
#endif
		}
	} else {
		printf("ERROR INVALID GATE AT RANDOMGATE r: %d\n", r);
		return;
	}

	if(gateSize == 2 && ctrlqidx != qidx) {
		len += snprintf(buf+len, MAX, "%d, %d ", ctrlqidx, qidx);
		twoQubitUpdate(s, *gate, ctrlqidx, qidx);
	} else {
		len += snprintf(buf+len, MAX, "%d ", qidx);
		singleQubitUpdate(s, *gate, qidx);
	}
	printf("%-20s", buf);
}

void doRandomCircuit(State *s)
{
	int32_t i;
	allHadamards(s);
	for(i = 0; i < MAX_RAND_NUMS; i++) {
		randomGate(s, randGates[i], randQubits[i],
				randAngles[i], randCtrlQubits[i],
				randGateSize[i]);
		// calcLedColours(s, randQubits[i]);
	}
}

void doRandomGate(State *s)
{
	s->randomGateIdx++;
	if (s->randomGateIdx >= MAX_RAND_NUMS) {
		s->randomGateIdx = 0;
	}
	int i = s->randomGateIdx;
	randomGate(s, randGates[i], randQubits[i], randAngles[i], randCtrlQubits[i], randGateSize[i]);
}

void randCircuitBenchmark(State *s)
{
	int32_t i;
	// setup(s);
	printv(s);
	allHadamards(s);
	// prints(s);
	// return;
	for(i = 0; i < MAX_RAND_NUMS; i++) {
		randomGate(s, randGates[i], randQubits[i],
				randAngles[i], randCtrlQubits[i],
				randGateSize[i]);
	}
	for(i = MAX_RAND_NUMS - 1; i >= 0; i--) {
		randomGate(s, randGates[i], randQubits[i], -randAngles[i],
				randCtrlQubits[i], randGateSize[i]);
	}
	printf("After random circuit of %d gates and inverse of %d gates\n",
			MAX_RAND_NUMS, MAX_RAND_NUMS);
	allHadamards(s);
	printv(s);
}

int controllerInput(void)
{
	struct js_event {
		uint32_t time;
		int16_t value;
		uint8_t type, number;
	} input = {.time = 0, .value = 0, .type = 0, .number = 0};
	FILE *usb;
	usb = fopen("/dev/input/js0", "r");
	if (!usb) {
		return -1;
	}
	// fread(&input, sizeof(input), 1, usb);

	// fread(&input.time, sizeof(input.time), 1, usb);
	// fread(&input.value, sizeof(input.value), 1, usb);
	// fread(&input.type, sizeof(input.type), 1, usb);
	// fread(&input.number, sizeof(input.number), 1, usb);

	printf("Controller input:\ntime: %d\nvalue: %d\ntype: %d\nnumber: %d\n",
			input.time, input.value, input.type, input.number);

	fclose(usb);
	return 0;
}

void interactiveMode(State *s)
{
	char buf[INPUT_BUF_SIZE];
	while (1) {
		keyboardInput(buf);
		parseArgs(s, buf);
		printQubit(s);
	}
}

int32_t interactiveTest(void)
{
	int32_t result;
	char userInput[INPUT_BUF_SIZE];

	// while (1) {
	// controllerInput();
	// }

	State *s;
	s = makeState();
	printf("sizeof(v) / (1024 * 1024): %lud MB\n", sizeof(*s) / (1024 *1024));
	printf("%lud\n", MAX_STATE_VECT_SIZE * sizeof(vtype) / (1024*1024));

	// randCircuitBenchmark(s);
	// writeRandomNumsToDisk();

	// int32_t i;
	// singleQubit(s, H, 0);
	// for(i = 0; i < NUM_QUBITS-1; i++) {
	// 	// singleQubitReal(v, X, i);
	// 	twoQubit(s, X, i, i+1);
	// }
	// printv(s);

	setVacuum(s);
	allHadamards(s);

	// doRandomCircuit(s);
	printv(s);
	// printQubit(s);

	result = s->v[0];

	free(s);

	s = makeState();
	printQubit(s);
	while (1) {
		keyboardInput(userInput);
		parseArgs(s, userInput);
		// printQubit(s);
		printQubit(s);
	}

	return result;
}

void colourTest(void)
{
	uint8_t hsl[3], rgb[3];
	uint16_t i;
	for (i = 0; i < 255; i+=10) {
		hsl[0] = i;
		hsl[1] = 255;
		hsl[2] = LIGHTNESS_AVG;

		hsl2rgb(hsl, rgb);

		trueColour(rgb);
		printf("Hue: %3d R: %3d, G: %3d, B: %3d\n",
				hsl[0],
				rgb[0], rgb[1], rgb[2]);
		printf(CRESET);
	}
}
