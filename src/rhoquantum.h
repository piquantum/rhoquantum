/**
 * @author      : oli (oli@$HOSTNAME)
 * @file        : defc
 * @created     : Friday Mar 01, 2024 19:16:50 GMT
 */

#ifndef DEFC_H
#define DEFC_H

#include <stdint.h>

#define USE_FLOATS
// #define USE_FLOATS_FOR_COLOURS

#ifdef USE_FLOATS
#define USE_FLOATS_FOR_COLOURS
#endif

#define NUM_QUBITS 		19 // 19 is max?
// #define NUM_QUBITS 		8 // 19 is max?
#define MAX_STATE_VECT_SIZE 	(1 << (NUM_QUBITS + 1))
#define VSIZE 			(MAX_STATE_VECT_SIZE >> 1)

// These are for user input in parseArgs()
// there are at most 8 arguments, realistically the input should be like 4
// args max but we do 8 for safety
#define MAX_NUM_ARGS   8
// if the argument is larger than 8 significant figures we have issues with
// the user ;)
#define MAX_ARG_SIZE   8
#define MAX_SAVED_CMDS 128 // for interactive mode
#define INPUT_BUF_SIZE (MAX_NUM_ARGS * MAX_ARG_SIZE)

// for now assume all matrices are 2x2 complex valued and row major
#ifdef USE_FLOATS
typedef float Matrix2[8];
#define vtype float
// #define vtype volatile float
#else

// typedef unsigned char int8_t;
// typedef unsigned char uint8_t;
// typedef short int int16_t;
// typedef int int32_t;
// typedef unsigned int uint32_t;

typedef int32_t Matrix2[8];
// define PRECISION to use 16bit 0.16 fixed point
#define vtype int16_t
// #define vtype volatile int16_t
#endif

extern uint32_t SEED;
extern Matrix2 X, Y, Z, H, T;
extern Matrix2 Rx, Ry, Rz;

typedef struct {
	// NUM_QUBITS is log2 size of vector
	// State visualisation
	uint8_t rgbColours[NUM_QUBITS][3]; // r, g, b
	uint8_t hslColours[NUM_QUBITS][3]; // h, s, l
	// This is for saving the current position of the state within the
	// state vector when we cycle through the non-zero elements in
	// nextBasisState()
	int32_t basisState;
	int32_t randomGateIdx;
	// These are for the partial density matrices, used in generating the
	// colour maps for the state
	float phi[NUM_QUBITS];
#ifdef USE_FLOATS_FOR_COLOURS
	float m[NUM_QUBITS][2], p[NUM_QUBITS][2];
#else
	int32_t m[NUM_QUBITS][2], p[NUM_QUBITS][2];
#endif
	// for command history
	uint8_t lastCmdLen[MAX_SAVED_CMDS];
	char cmdHist[MAX_SAVED_CMDS][MAX_NUM_ARGS][MAX_ARG_SIZE];
	// The actual state vector
#ifdef SDRAM
	vtype *v;
#else
#ifdef USE_FLOATS
	float v[MAX_STATE_VECT_SIZE];
#else
	vtype v[MAX_STATE_VECT_SIZE];
#endif // USE_FLOATS
#endif // either use the sdram or allocate vector on the stack
} State;

State *makeState(void);

// random numbers
float randf(void);
int rand_max(int max);
float randf(void);

void setRx(float twoa);
void setRy(float twoa);
void setRz(float twoa);

// this only does the gate
void singleQubit(State *s, Matrix2 m, uint8_t idx);
void twoQubit(State *s, Matrix2 m, uint8_t ctrl, uint8_t targ);
// this does the gate AND updates the led colours in state struct
void singleQubitUpdate(State *s, Matrix2 m, uint8_t idx);
void twoQubitUpdate(State *s, Matrix2 m, uint8_t ctrl, uint8_t targ);

void setVacuum(State *s);
void setAllPlusState(State *s);
void allHadamards(State *s);

void doRandomCircuit(State *s);
void randCircuitBenchmark(State *s);
void writeRandomNumsToDisk(void);
void doRandomGate(State *s);

// user input
void keyboardInput(char buf[INPUT_BUF_SIZE]);
void parseArgs(State *s, const char buf[INPUT_BUF_SIZE]);

void printv(State *s);
void printm(Matrix2 m);
void printQubitAmplitudes(State *s);
void printQubit(State *s);
void printPartialDensityMatrix(State *s);

void hsl2rgb(uint8_t *hsl, uint8_t *rgb);
void trueColour(uint8_t *rgb);

// funcs that main might call

int controllerInput(void);
void interactiveMode(State *s);
int32_t interactiveTest(void);
void colourTest(void);

//Regular text
#define BLK "\33[0;30m"
#define RED "\33[0;31m"
#define GRN "\33[0;32m"
#define YEL "\33[0;33m"
#define BLU "\33[0;34m"
#define MAG "\33[0;35m"
#define CYN "\33[0;36m"
#define WHT "\33[0;37m"

#define CRESET "\033[0m"
#define COLOR_RESET "\033[0m"

#endif /* end of include guard DEFC_H */
