# Intro
The c source code consists of `rhoquantum.h`, `rhoquantum.c` which implements
all of the logic for the quantum state simulation. To run the c simulator in
interactive mode, build then run `./main`.

Currently we also support python bindings which imports the c source code
shared library `librhoquantum.so`, the python code is found in `pymain.py`.
You need to build the shared library first using `make` then you can run the
python version with `python3 pymain.py`.

# Building
You can call `make` to build the shared library `librhoquantum.so` in the
current directory. The default `make` target builds in release mode using `-Os
-flto`. There is a debug build available which is built using `make build`.

Main is in `main.c` which links to the shared library, this is all taken care
of in the make file.
