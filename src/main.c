/**
 * @author      : oli (oli@$HOSTNAME)
 * @file        : main
 * @created     : Thursday Apr 18, 2024 20:32:00 BST
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rhoquantum.h"

int main()
{
	int32_t result;
	uint8_t i, j;
	SEED = 0xcafe;

	colourTest();

	State *s = makeState();
	// printf("sizeof(v) / (1024 * 1024): %ld MB\n", sizeof(s) / (1024 *1024));
	// printf("%ld\n", MAX_STATE_VECT_SIZE * sizeof(fixedprec) / (1024*1024));

	doRandomCircuit(s);

	// printv(s);
	// printQubit(s);

	setVacuum(s);
	// uint8_t qidx[8] = {1, 5, 10, 14, 17, 13, 8, 4};
	uint8_t qidx[8] = {0, 1, 2, 3, 4, 5, 6, 7};
	for (i = 0; i < 8; i++) {
		singleQubit(s, H, qidx[i]);
		for (j = 0; j < i; j++) {
			singleQubit(s, T, qidx[i]);
		}
	}

	printQubitAmplitudes(s);
	printPartialDensityMatrix(s);
	printQubit(s);
	// interactiveMode(s);

	result = s->v[0];

	free(s);

	return result;
}
