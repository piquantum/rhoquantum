#include "sdram.h"
// #include "cmsis_os2.h"

static FMC_SDRAM_CommandTypeDef Command;
extern SDRAM_HandleTypeDef hsdram1; // must match name in Core/Src/main.c

void sdram_init_sequence(uint32_t refresh_count)
{
  __IO uint32_t tmpmrd =0;

  /* Step 1:  Configure a clock configuration enable command */
  Command.CommandMode             = FMC_SDRAM_CMD_CLK_ENABLE;// CubeMX
  Command.CommandTarget           = FMC_SDRAM_CMD_TARGET_BANK1; // CubeMX
  Command.AutoRefreshNumber       = 1;
  Command.ModeRegisterDefinition  = 0;

  /* Send the command */
  HAL_SDRAM_SendCommand(&hsdram1, &Command, SDRAM_TIMEOUT); // BSP: 0xFFFF

  /* Step 2: Insert 100 us minimum delay */
  /* Inserted delay is equal to 1 ms due to systick time base unit (ms) */
  // osDelay(1);
  HAL_Delay(1);

  /* Step 3: Configure a PALL (precharge all) command */
  Command.CommandMode             = FMC_SDRAM_CMD_PALL;// CubeMX
  Command.CommandTarget           = FMC_SDRAM_CMD_TARGET_BANK1; // CubeMX
  Command.AutoRefreshNumber       = 1;
  Command.ModeRegisterDefinition  = 0;

  /* Send the command */
  HAL_SDRAM_SendCommand(&hsdram1, &Command, SDRAM_TIMEOUT); // BSP: 0xFFFF

  /* Step 4: Configure an Auto Refresh command */
  Command.CommandMode             = FMC_SDRAM_CMD_AUTOREFRESH_MODE; // CubeMX
  Command.CommandTarget           = FMC_SDRAM_CMD_TARGET_BANK1; // CubeMX
  Command.AutoRefreshNumber       = 4;
  Command.ModeRegisterDefinition  = 0;

  /* Send the command */
  HAL_SDRAM_SendCommand(&hsdram1, &Command, SDRAM_TIMEOUT); // BSP: 0xFFFF

  /* Step 5: Program the external memory mode register */
  tmpmrd = (uint32_t)SDRAM_MODEREG_BURST_LENGTH_1          |// BSP
                     SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL   |// BSP
                     SDRAM_MODEREG_CAS_LATENCY_3           |// BSP
                     SDRAM_MODEREG_OPERATING_MODE_STANDARD |// BSP
                     SDRAM_MODEREG_WRITEBURST_MODE_SINGLE;// BSP

  Command.CommandMode             = FMC_SDRAM_CMD_LOAD_MODE;// CubeMX
  Command.CommandTarget           = FMC_SDRAM_CMD_TARGET_BANK1;// CubeMX
  Command.AutoRefreshNumber       = 1;
  Command.ModeRegisterDefinition  = tmpmrd;// local

  /* Send the command */
  HAL_SDRAM_SendCommand(&hsdram1, &Command, SDRAM_TIMEOUT); // BSP

  /* Step 6: Set the refresh rate counter */
  /* Set the device refresh rate */
  HAL_SDRAM_ProgramRefreshRate(&hsdram1, refresh_count);// local
}
