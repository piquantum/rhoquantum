# RhoQuantum

Python library supporting development and use of the RhoQuantum "Quantum
Computer".

To set up for development on Linux, follow these steps (from this directory):

```bash
# Create a new virtual environment (first time only)
python3 -m venv venv

# Activate the environment (every time you open a new terminal)
source venv/bin/activate

# Install rhoquantum for development inside the environment
pip install -e .

# Install MkDocs to build the documentation
pip install mkdocs mkdocs-material mkdocstrings[python]

# Build the documents for live editing
mkdocs serve
```

You can now write scripts that `import rhoquantum` (make sure the environment
is active; run them using `python script.py`), or run `python` and import it
interactively.
