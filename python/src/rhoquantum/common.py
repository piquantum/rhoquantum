import numpy as np
from qiskit.quantum_info import Statevector, DensityMatrix, partial_trace
from pygame import Color

def insert(x: int, pos: int, bit: int):
    """Insert bit at position pos of integer x
    """
    mask = 2**pos-1
    lower = x & mask
    upper = (x & ~mask) << 1
    return upper + (bit << pos) + lower

def print_state_vector(state: Statevector):
    """Print a qiskit state vector vertically

    This is a convenience function to print the
    state vector amplitudes and their index vertically.

    Args:
        state: The state to print
    """
    print("[")
    for index, elem in enumerate(state):
        index_string = f"{index:b}".zfill(state.num_qubits)
        print(f" {index_string}: {elem}")
    print("]")

def make_all_zero_state(num_qubits: int):
    return Statevector.from_label("0"*num_qubits)

def one_qubit_mixed_state(state, qubit_index):
    num_qubits = state.num_qubits
    other_qubits = [n for n in range(num_qubits) if n != qubit_index]
    return partial_trace(state, other_qubits)

def print_state(state: Statevector, led_states: list):
    """Print the state of all qubits (state vector and density matrices)
    """
    print_state_vector(state)
    for n in range(num_qubits):
        print(f"Qubit {qubit_index} density matrix:")
        print(one_qubit_mixed_state(state, n))
    print(led_states)

def make_led_colour(mag: float, phase: float) -> Color:
    """Convert a magnitude and phase to an LED colour value

    The colour is created in HSV (hue, saturation, value) format. In
    this format, hue represents the angle around the colour wheel, and
    is set based on the phase. The value is set to the magnitude,
    which determines the brightness.

    The saturation is fixed at 100%, putting the colour on the edge of
    the colour wheel (which is really a disc).
    """
    if not (0 <= mag <= 1.0):
        raise RuntimeError(f"Got invalid magnitude {mag}")

    if not (0 <= phase <= 2*np.pi):
        raise RuntimeError(f"Got invalid phase {phase}")

    sat = 100 # Edge of colour wheel
    alpha = 100 # Transparency

    col = Color(0)
    hue = 360 * phase / (2 * np.pi) # Angle round colour wheel
    val = 100 * mag # Brightness of the colour
    col.hsva = (hue, sat, val, alpha)
    return col

def map_angle(angle: float) -> float:
    """Take an angle in the range (-pi, pi] and convert to [0, 2pi)

    Used to convert output from np.angle for use with colours.
    """

    if angle < 0:
        zero_to_2pi = angle + 2*np.pi
    else:
        zero_to_2pi = angle

    if zero_to_2pi < 0.0:
        zero_to_2pi = 0.0
    if zero_to_2pi > 2*np.pi:
        zero_to_2pi = 2*np.pi

    return zero_to_2pi
