"""Physical board geometry

This module contains calculations of the position of the LEDs and
screws on the board.
"""

from enum import Enum
from dataclasses import dataclass, field
import numpy as np
from pygame import Vector2
import matplotlib.pyplot as plt

@dataclass
class LayoutConfig:
    """Defines the physical geometry of the PCB and LED/screw layout

    All coordinates are in millimetres with coordinates based on (0,0)
    at the top-left, with the y-axis increasing downwards.

    All four PCBs have the same layout, but the top two PCBs are
    rotated through 180 degrees. The fifth PCB sits in the centre behind
    the four boards and is used to screw them together.

    Attributes:
        board_width: The PCB width in mm. The height is determined by
            the aspect ratio. The default value is 100mm, which is the
            maximum PCBWay board size before the boards get expensive.

        board_gap: This tolerance is required to account for any
            roughness/incorrect edge cuts and make the PCBs screw together
            more easily. A default tolerance of 1mm between boards is
            used, which should account for any PCB edge roughness/cut
            tolerance (PCBWay specifies 0.5mm tolerance for the board outline
            dimensions; i.e. 0.25mm on each size).

        aspect_ratio: In order to support the tiling triangular array of
            LEDs, the board aspect ratio must be the same as the ratio
            of the base to the height of an equilateral triangle, which is
            the default value.

        tft_display_diagonal: The vector from the top-left corner of the
            display to the bottom-right. This defines the spacing of the
            screws in each board quadrant, so that they line up with the
            screen. The default screen is DIYmalls 1.8" TFT LCD Display
            Screen Module (on Amazon).

        screw_hole_diam: The diameter of the drilled screw hole. The default
            is 3.5mm, for use with M3 screws.

        screw_hole_pad_diam: The diameter of the pad around the screw hole.
            The default is set to 6mm to ensure that it covers the footprint
            of the typical M3 screw head.

        led_diagonal: The vector from the top-left corner of the Neopixel
           LED to the bottom right. The LEDs are 5mm x 5mm, but the width
           is set to 7mm to reflect solder pads. (Note that LEDs are rotated
           before being drawn on the board.)

    """
    board_width: float = 100
    board_gap: float = 1
    aspect_ratio: float = np.sqrt(3) / 2
    tft_display_diagonal: Vector2 = field(default_factory=lambda:
                                          Vector2(33.66, 58.01))
    screw_hole_diam: float = 3.5
    screw_hole_pad_diam: float = 6
    led_diagonal: Vector2 = field(default_factory=lambda:Vector2(7, 6))

    def scale_by(self, factor: float) -> "LayoutConfig":
        """Return a new object with each dimension scaled by factor

        Mainly for the purpose of drawing the board for the emulator.
        """
        return LayoutConfig(
            board_width=factor*self.board_width,
            board_gap=factor*self.board_gap,
            tft_display_diagonal=factor*self.tft_display_diagonal,
            screw_hole_diam=factor*self.screw_hole_diam,
            screw_hole_pad_diam=factor*self.screw_hole_pad_diam,
            led_diagonal=factor*self.led_diagonal
        )

    def board_height(self) -> float:
        """PCB height
        """
        return self.board_width * self.aspect_ratio

    def board_diagonal(self) -> Vector2:
        """Vector from the top-left corner to the bottom-right
        """
        return Vector2(self.board_width, self.board_height())

    def tiling_diagonal(self) -> Vector2:
        """The tiling dimensions of the four boards

        This is the vector from the centre of the top-left board
        to the centre of the bottom-right board. It includes the
        board width/height and the board gap components.
        """
        return self.board_diagonal() + self.board_gap * Vector2(1,1)

    def get_led_coords(self) -> list[Vector2]:
        """Get the coordinates of the LEDs on the bottom-left PCB
        """
        return get_led_coords(
            self.board_diagonal(),
            self.board_gap
        )

    def get_screw_coords(self) -> list[Vector2]:
        """Get the coordinates of the screws on the bottom-left PCB
        """

        # The screw horizontal separation coincides with the
        # display width
        screw_x_sep = self.tft_display_diagonal[0]

        vboard_diagonal = get_vboard_diagonal(
            self.board_diagonal(),
            self.board_gap
        )
        led_sep_y = vboard_diagonal[1] / 3

        # To balance the minimum distance between LEDs and screws, set
        # this screw separation.
        screw_y_sep = led_sep_y

        return get_screw_coords(
            self.board_diagonal(),
            self.board_gap,
            screw_x_sep,
            screw_y_sep,
            self.tft_display_diagonal[1]
        )

# Board coordinates are from (0,0) at the top left (like graphics coordinates,
# and also how the PCB coordinates work). Virtual coordinates use the centre of
# the board as (0,0). In addition, the "virtual board" refers to a board extended
# by board_gap/2 on each side, so that the boards tile perfectly together.

# The board_diagonal is the vector from the top-left to the bottom-right of the
# (physical) board.

def get_vboard_diagonal(board_diagonal: Vector2, board_gap: float) -> Vector2:
    """Used to calculate virtual board width and height
    """
    return board_diagonal + board_gap * Vector2(1,1)

def v2b_coords(xy: Vector2, board_diagonal: Vector2) -> Vector2:
    """Convert from board to virtual board coordinates
    """
    return xy + board_diagonal / 2

def b2v_coords(xy: Vector2, board_diagonal: Vector2) -> Vector2:
    """Convert from virtual to board coordinates
    """
    return xy - board_diagonal / 2

class Quadrant(Enum):

    """Defines a quadrant of the (virtual) board

    Quadrant coordinates are a system where the origin is the centre of the
    quadrant of the virtual board. There are four quadrant coordinate systems
    for each board, which nearly coincide with the actual quadrants of the board
    (The discrepancy is because the virtual coordinate system starts slightly
    outside the board.) The quadrants are labelled with integers:

    0: 00, top left
    1: 01, bottom left
    2: 10, top right
    3: 11, bottom right

    """
    TOP_LEFT = 0
    BOTTOM_LEFT = 1
    TOP_RIGHT = 2
    BOTTOM_RIGHT = 3

def v2q_offset(quadrant: Quadrant, quad_diagonal: Vector2) -> Vector2:
    match quadrant:
        case Quadrant.TOP_LEFT:
            return Vector2(-1,-1).elementwise() * quad_diagonal / 2
        case Quadrant.BOTTOM_LEFT:
            return Vector2(-1,1).elementwise() * quad_diagonal / 2
        case Quadrant.TOP_RIGHT:
            return Vector2(1,-1).elementwise() * quad_diagonal / 2
        case Quadrant.BOTTOM_RIGHT:
            return Vector2(1,1).elementwise() * quad_diagonal / 2
        case other:
            raise ValueError(f"Invalid argument {other} for quadrant")

def v2q_coords(xy: Vector2, quadrant: Quadrant, quad_diagonal: Vector2) -> Vector2:
    """Convert from virtual to quadrant coordinates
    """
    return xy - v2q_offset(quadrant, quad_diagonal)

def q2v_coords(xy: Vector2, quadrant: Quadrant, quad_diagonal: Vector2) -> Vector2:
    """Convert from quadrant to virtual coordinates
    """
    return xy + v2q_offset(quadrant, quad_diagonal)

def get_quad_diagonal(board_diagonal: Vector2, board_gap: float) -> Vector2:
    return get_vboard_diagonal(board_diagonal, board_gap) / 2

def plot_coords(coords: list[Vector2], **kwargs):
    x, y = zip(*coords)
    plt.scatter(x, y, **kwargs)

def plot_edge_cuts(board_width: float, board_height: float, offset: Vector2):

    x = [x + offset[0] for x in [0, 0, board_width, board_width, 0]]
    y = [y + offset[1] for y in [0, board_height, board_height, 0, 0]]

    plt.plot(x, y, color="k")

def get_screw_quad_coords(x_sep: float, y_sep: float) -> list[Vector2]:
    """Get the coordinates of four screws located symmetrically
    in the virtual quadrant coordinates. The order of the screws is:

    0: top left
    1: bottom left
    2: top right
    3: bottom right
    """

    return [
        Vector2(-x_sep/2, -y_sep/2),
        Vector2(-x_sep/2, +y_sep/2),
        Vector2(+x_sep/2, -y_sep/2),
        Vector2(+x_sep/2, +y_sep/2),
    ]


def get_screw_coords(board_diagonal: Vector2, board_gap: float, x_sep: float, y_sep: float, tft_height: float):
    """Lay out the screw coordinates on the board
    """
    quad_size = get_quad_diagonal(board_diagonal, board_gap)

    # Add the screws for each quadrant.
    coords = []
    for quad in Quadrant:
        # In each quadrant, add four screws
        for quad_xy in get_screw_quad_coords(x_sep, y_sep):
            virt_xy = q2v_coords(quad_xy, quad, quad_size)
            board_xy = v2b_coords(virt_xy, board_diagonal)
            coords.append(board_xy)

    # Add the screws for the other side of the TFT display. Note
    # that these are offset from the two bottom screws in the
    # bottom right quadrant
    coords.append(coords[13] - Vector2(0, tft_height))
    coords.append(coords[15] - Vector2(0, tft_height))

    return coords

def get_led_coords(board_diagonal: Vector2, board_gap: float):
    """Get the board coordinates of the centre of the LEDs

    LEDs are indexed from the top to the bottom in a snake pattern,
    starting with the top left. This matches the electrical connection
    order of the neopixels.
    """
    vboard_diagonal = get_vboard_diagonal(board_diagonal, board_gap)
    led_sep_x, led_sep_y = vboard_diagonal / 3

    # Start at the top left (virtual coordinates from the centre
    # of the board)
    position = Vector2(-1.25*led_sep_x, -led_sep_y)

    # List of displacements to each subsequent LEDs
    displacements = [
        Vector2(0,0),
        Vector2(led_sep_x, 0),
        Vector2(led_sep_x, 0),
        Vector2(led_sep_x / 2, led_sep_y),
        Vector2(-led_sep_x, 0),
        Vector2(-led_sep_x, 0),
        Vector2(-led_sep_x / 2, led_sep_y),
        Vector2(led_sep_x, 0),
        Vector2(led_sep_x, 0),
    ]

    # Calculate all virtual board coordinates
    virt_coords = []
    for disp in displacements:
        position += disp
        virt_coords.append(position.copy())

    # Convert to board coords
    coords = [v2b_coords(xy, board_diagonal) for xy in virt_coords]

    return coords

def rotate_coords(coords: list[Vector2], board_diagonal: Vector2) -> list[Vector2]:
    """Rotate a list of board coordinates through 180 degrees on the board
    """

    v_coords = [b2v_coords(xy, board_diagonal) for xy in coords]
    rotated_coords = [xy.rotate(180) for xy in v_coords]
    return [v2b_coords(xy, board_diagonal) for xy in rotated_coords]
