"""Layout items on the PCB
"""

try:
    # Put these here so that users running layout_pcb -p can
    # run the script without kicad installed.
    from pcbnew import LoadBoard, VECTOR2I_MM, EDA_ANGLE
except:
    print("NOTE: Python module `pcbnew` not found. Most likely, Kicad is")
    print("not installed. Alternatively, PYTHONPATH may be wrong, or you ")
    print("may be using a virtual environment, which is not possible when")
    print("using Kicad `pcbnew`. Reading/writing PCB files will not work.")
    print()
    print("Refer to rhoquantum/hardware/README.adoc for more information.")
    print()
    
# Suppressing pygame message so as not to confuse with
# argparse help/command line arguments. Would also be possible
# to remove pygame from the geometry module, it isn't really
# doing anything other than adding the Vector2 class for
# convenience.
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import argparse
import numpy as np
import matplotlib.pyplot as plt
from pygame import Vector2

from rhoquantum.hardware import geometry

description = """
Take an existing Kicad PCB file containing existing footprints,
and move the footprints to calculated positions.

EXAMPLES

# Plot the boards (for verification) and exit (no layout performed)
layout_pcb -p

# Layout the LEDs and screws on the PCB
layout_pcb -i rhoquantum.kicad_pcb -o rhoquantum_placed

DETAIL

More detail please...

"""


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    pass


def script_entry():

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=CustomFormatter)
    parser.add_argument("-p",
                        "--plot-layout",
                        help="Plot a graph of the component layout for verification",
                        action="store_true")
    parser.add_argument("-v",
                        "--verbose",
                        help="Print the coordinates of the LEDs and screws",
                        action="store_true")
    parser.add_argument("-i",
                        "--input-file",
                        help="the path to the input *.kicad_pcb file")
    parser.add_argument("-o",
                        "--output-file",
                        help="the root name of the output PCB file",
                        default="placed")

    args = parser.parse_args()

    config = geometry.LayoutConfig()
    screw_coords = config.get_screw_coords()
    led_coords = config.get_led_coords()
    
    if args.verbose:
        print("LED Coordinates:")
        [print(f" {xy}") for xy in led_coords]
        
        print("Screw Coordinates:")
        [print(f" {xy}") for xy in screw_coords]
        
    if args.plot_layout:
        plot_layout(
            screw_coords,
            led_coords,
            config.board_diagonal(),
            config.board_gap
        )
        return

    # Load the board
    if args.input_file is None:
        print("Invalid arguments")
        print()
        print("You must supply the path to the input *.kicad_pcb file")
        print("using -i in order to do the layout. Alternatively, use")
        print("-p to plot the coordinates for verification.")
        print()
        print("Use --help for more information.")
        return

    try:
        pcb = LoadBoard(args.input_file)
    except Exception as e:
        print(f"Error '{e}' while trying to open PCB file '{args.input_file}'.")
        return


    # The code assumes the reference designators for the screw holes
    # (TPn), the LEDs (Dn), and the LED jumpers (JPn) are contiguous.
    # The starting value for each is included in the parameters for
    # this program
    led_start = 2 # D2
    jumper_start = 2 # JP2
    screw_hole_start = 1 # TP1
    led_cap_start = 41 # C41
    
    # Move the screw holes on the board
    for n in range(len(screw_coords)):

        # Find the screw hole (note they are TP1, TP2, ...)
        ref = f"TP{screw_hole_start+n}"
        c = pcb.FindFootprintByReference(ref)

        # Place it
        pos = screw_coords[n]
        c.SetPosition(VECTOR2I_MM(pos[0],pos[1]))

    # Move the screen into position. The position is the
    # average of the last two screw positions (for the screen)
    # with the bottom-most screws on the right side of the board.
    ref = "U8"
    c = pcb.FindFootprintByReference(ref)
    pos = (screw_coords[-1] + screw_coords[-2] + screw_coords[-3] + screw_coords[-5]) / 4
    c.SetPosition(VECTOR2I_MM(pos[0],pos[1]))
    c.SetOrientation(EDA_ANGLE(180))
    
    if args.verbose:
        print("Screen position:")
        print(pos)
    
    # Move the LEDs on the board
    for n in range(len(led_coords)):

        # Each LED is offset by half the size of the LED vertical
        # footprint size (since the LEDs are rotated). Ensure that
        # this parameter includes some tolerance to separate the LEDs.
        led_offset_x = config.led_diagonal[1] / 2
        
        # Calculate the location of the |0) and |1) LEDs
        pos_0 = led_coords[n] - Vector2(led_offset_x, 0)
        pos_1 = led_coords[n] + Vector2(led_offset_x, 0)

        # Find the LED bypass jumper reference (note they are JP1, JP2, ...)
        ref = f"JP{jumper_start+n}"
        c = pcb.FindFootprintByReference(ref)

        # Place it under LED 0
        c.SetPosition(VECTOR2I_MM(pos_0[0],pos_0[1]))

        # Find LED 0
        ref = f"D{led_start + 2*n}"
        c = pcb.FindFootprintByReference(ref)

        # Place it and set orientation
        c.SetPosition(VECTOR2I_MM(pos_0[0],pos_0[1]))
        c.SetOrientation(EDA_ANGLE(90))

        # Find the first capacitor
        ref = f"C{led_cap_start + 2*n}"
        c = pcb.FindFootprintByReference(ref)

        # Place it under LED 0
        c.SetPosition(VECTOR2I_MM(pos_0[0],pos_0[1]))
        
        # Find LED 1
        ref = f"D{led_start + 2*n + 1}"
        c = pcb.FindFootprintByReference(ref)

        # Place it and set orientation
        c.SetPosition(VECTOR2I_MM(pos_1[0],pos_1[1]))
        c.SetOrientation(EDA_ANGLE(90))

        # Find the second capacitor
        ref = f"C{led_cap_start + 2*n + 1}"
        c = pcb.FindFootprintByReference(ref)

        # Place it under LED 1
        pos = led_coords[n]
        c.SetPosition(VECTOR2I_MM(pos_1[0],pos_1[1]))
        
    # Save the PCB file
    pcb.Save(args.output_file + ".kicad_pcb")


def plot_layout(screw_coords: list[Vector2], led_coords: list[Vector2], board_diagonal: Vector2, board_gap: float):
    """Verify the layout of the screws/LEDs by plotting the boards
    """

    board_width = board_diagonal[0]
    board_height = board_diagonal[1]
    
    aspect_ratio = board_height / board_width
    
    x_displace = board_width + board_gap
    y_displace = board_height + board_gap

    # Four tiled boards
    screw_coords_00 = screw_coords
    screw_coords_01 = [xy + Vector2(x_displace, 0) for xy in screw_coords_00]
    rotated_screw_coords = geometry.rotate_coords(screw_coords_00, board_diagonal)
    screw_coords_10 = [xy + Vector2(0, -y_displace) for xy in rotated_screw_coords]
    screw_coords_11 = [xy + Vector2(x_displace, -y_displace) for xy in rotated_screw_coords]

    # Calculate LED position
    led_coords_00 = led_coords
    led_coords_01 = [xy + Vector2(x_displace, 0) for xy in led_coords_00]
    rotated_led_coords = geometry.rotate_coords(led_coords_00, board_diagonal)
    led_coords_10 = [xy + Vector2(0, -y_displace) for xy in rotated_led_coords]
    led_coords_11 = [xy + Vector2(x_displace, -y_displace) for xy in rotated_led_coords]

    # Middle board screwing the others together
    screw_coords_middle = [xy + Vector2(x_displace / 2, -y_displace / 2) for xy in screw_coords_00]

    #quad = Vector2(0,1)
    #screw_coords = [q2v_screw_coords(xy, quad, quad_size) for xy in get_screw_q_screw_coords(10, 10)]
    geometry.plot_coords(screw_coords_00, s=30)
    geometry.plot_coords(screw_coords_01, s=30)
    geometry.plot_coords(screw_coords_10, s=30)
    geometry.plot_coords(screw_coords_11, s=30)
    geometry.plot_coords(screw_coords_middle, s=10, color="white")

    # Plot the location of the LEDs
    geometry.plot_coords(led_coords_00, s=50, marker=">")
    geometry.plot_coords(led_coords_01, s=50, marker=">")
    geometry.plot_coords(led_coords_10, s=50, marker=">")
    geometry.plot_coords(led_coords_11, s=50, marker=">")

    # Plot lines through the LEDs to verify they are in line. This is
    # visual verification that the LEDs do lie on a triangular array.
    for coords in [led_coords_00, led_coords_01, led_coords_10, led_coords_11]:
        for n in range(9):
            for slope in [0, 2*aspect_ratio, -2*aspect_ratio]:
                plt.axline(coords[n], slope=slope, linewidth = 0.05)


    # To check everything is within the board boundary
    geometry.plot_edge_cuts(board_width, board_height, Vector2(0,0))
    geometry.plot_edge_cuts(board_width, board_height, Vector2(x_displace, 0))
    geometry.plot_edge_cuts(board_width, board_height, Vector2(0, -y_displace))
    geometry.plot_edge_cuts(board_width, board_height, Vector2(x_displace, -y_displace))
    geometry.plot_edge_cuts(board_width, board_height, Vector2(x_displace / 2, -y_displace / 2))

    plt.axis('equal')
    plt.gca().invert_yaxis()
    plt.title("Layout of LEDs (triangles) and screws (circles) on the five boards")
    plt.show()

    
