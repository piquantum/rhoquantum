"""Emulate PiQuantum/RhoQuantum LED display

The purpose of this code is to test whether the algorithms used to
calculate the state of the LEDs are reasonable when actually using
RhoQuantum.

A second purpose is to provide a concrete reference implementation for
the algorithms we will implement in RhoQuantum.

"""
