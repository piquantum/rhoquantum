# Remove library message from pygame which interferes with
# help message clarity
import os

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

import argparse
import pygame
from rhoquantum.emulate.inputs import keyboard_mouse, controller
from rhoquantum.emulate.leds import mixed_state, average_phases
from rhoquantum.emulate import state

description = """
Emulate RhoQuantum

EXAMPLES

# Run the emulator with default mouse/keyboard input
rq_emulate

# Run the emulator using a game controller input
rq_emulate -c

DETAIL

More detail please...

"""


class CustomFormatter(
    argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter
):
    pass


def draw_text(screen: pygame.Surface):
    title = pygame.freetype.SysFont("Comic Sans MS", 30)
    title.render_to(screen, (10, 10),
                      "RhoQuantum", fgcolor="black",
                      bgcolor="white")

    lines = [
        "Arrow keys: Select a qubit (q to quit)",
        "Single-qubit Discrete Ops.: X (x), H (h), Measure (m).",
        "Single-qubit Continuous Ops.: Rz (z+mouse), Ry (y+mouse)",
        "To do a controlled-gate: Press c; Pick target; Do one-qubit op. (as above)",

    ]

    instructions = pygame.freetype.SysFont("Comic Sans MS", 15)
    for (n, line) in enumerate(lines):
        instructions.render_to(screen, (10, 40 + 20*n),
                               line, fgcolor="black",
                               bgcolor="white")




def script_entry():
    """Main entry point for emulation script"""

    parser = argparse.ArgumentParser(
        description=description, formatter_class=CustomFormatter
    )
    parser.add_argument(
        "-c",
        "--controller-input",
        help="Use a game controller as input instead of mouse/keyboard",
        action="store_true",
    )
    parser.add_argument(
        "-a",
        "--average-phases",
        help="Enable average phases instead of mixed-state based LEDs (laggy/wrong)",
        action="store_true",
    )

    args = parser.parse_args()

    # pygame setup
    pygame.init()
    screen = pygame.display.set_mode((750, 750))
    clock = pygame.time.Clock()
    dt = 0

    # Qubit array size (proportion of screen)
    width = 0.7
    height = 0.7

    if args.controller_input:
        user_input = controller.UserInput()
    else:
        user_input = keyboard_mouse.UserInput(screen)

    if args.average_phases:
        calculate_led_state = average_phases.calculate_led_states
    else:
        calculate_led_state = mixed_state.calculate_led_state

    rho_quantum_state = state.RhoQuantumState(calculate_led_state)

    quit_sim = False
    while not quit_sim:

        # fill the screen with a color to wipe away anything from last frame
        screen.fill("white")

        draw_text(screen)

        rho_quantum_state.draw(screen, width, height)

        quit_sim, rho_quantum_input = user_input.read()

        rho_quantum_state.update(screen, rho_quantum_input)

        pygame.display.flip()  # to update the screen

        dt = clock.tick(60) / 1000
        rho_quantum_state.blink.update(dt)

    pygame.quit()
