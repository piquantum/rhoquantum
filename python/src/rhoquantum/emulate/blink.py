"""Control the blinking of selected qubit LEDs

This class provides options for setting the rate/duty cycle of the
blink, and also supports blinking-rate increase while controlled
operations are in progress. An important feature of the blinking is
that a newly selected LED should be immediately set to the off state,
because this makes the user interface feel responsive.
"""

from dataclasses import dataclass

@dataclass
class BlinkControl:
    base_period: float = 0.5
    duty: float = 0.8 # On-time proportion
    fast_speedup: float = 2 # Blink rate increase for controlled gate

    timer: float = 0
    led_on: bool = False # Whether blinking LEDs are on or off
    fast: bool = False # Fast blinking mode

    def set_off(self):
        """Reset the timer so that the flashing LEDs are off

        This does not change the fast/slow state. It is used
        for set the LED to off after the selected qubit has been
        changed, to ensure that the newly selected qubit is
        immediately obvious (changes from on to off immediately)
        """
        self.timer = 0
        self.led_on = False

    def set_fast(self, state: bool):
        """Set the blinking to fast or slow

        Use this method, because it also resets the timer
        """
        if self.fast != state:
            self.fast = state
            self.timer = 0

    def update(self, dt: float):
        """Update the internal timers/state based on elapsed time
        """

        if self.fast:
            on_time = self.base_period * self.duty / self.fast_speedup
            off_time = self.base_period * (1 -self.duty) / self.fast_speedup
        else:
            on_time = self.base_period * self.duty
            off_time = self.base_period * (1 -self.duty)

        if self.led_on and self.timer > on_time:
            self.led_on = False
            self.timer = 0
        elif not self.led_on and self.timer > off_time:
            self.led_on = True
            self.timer = 0
        else:
            self.timer += dt
