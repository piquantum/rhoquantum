import numpy as np
import pygame

from qiskit.quantum_info import partial_trace, Statevector, DensityMatrix
from qiskit.circuit import QuantumCircuit

from rhoquantum.common import print_state_vector, make_all_zero_state
from rhoquantum.emulate.blink import BlinkControl
from rhoquantum.emulate.inputs import RhoQuantumInput, keyboard_mouse, controller
from rhoquantum.emulate import draw
from rhoquantum.hardware import geometry

from dataclasses import dataclass, field
from pygame import Vector2, Color
from typing import Callable

import rhoquantum.emulate.sharedlib as lib

def do_one_qubit_op(state: Statevector, op: str, target: int, param: float | None = None) -> Statevector:
    """Perform a one-qubit gate on the specified qubit of the state vector

    Returns:
        The new state vector after applying the gate
    """
    qc = QuantumCircuit(state.num_qubits)
    match op:
        case "op_x":
            #print(f"Doing X on q{target}")
            qc.x(target)
            lib.singleQubit(lib.s, lib.X, target)
        case "op_h":
            #print(f"Doing H on q{target}")
            qc.h(target)
            lib.singleQubit(lib.s, lib.H, target)
        case "op_rz":
            if param is not None:
                #print(f"Doing Rz({param:.2f}) on q{target}")
                qc.p(param, target)
                lib.setRz(param / (2.0 * np.pi))
                lib.singleQubit(lib.s, lib.Rz, target)
            else:
                raise RuntimeError("Applying Rz requires a parameter")
        case "op_ry":
            if param is not None:
                #print(f"Doing Ry({param:.2f}) on q{target}")
                qc.ry(param, target)
                lib.setRy(param / (2.0 * np.pi))
                lib.singleQubit(lib.s, lib.Ry, target)
            else:
                raise RuntimeError("Applying Ry requires a parameter")
        case "op_measure":
            _, state = state.measure([target])
            return state

        case _:
            raise RuntimeError(f"One-qubit operation '{op}' not recognised")

    return state.evolve(qc)

def do_controlled_op(state: Statevector, op: str, control: int, target: int, param: float | None = None) -> Statevector:
    qc = QuantumCircuit(state.num_qubits)
    match op:
        case "op_x":
            print(f"Doing CNOT on q{control} (C) and q{target} (T)")
            qc.cx(control, target)
        case "op_h":
            print(f"Doing CH on q{control} (C) and q{target} (T)")
            qc.ch(control, target)
        case "op_rz":
            if param is not None:
                #print(f"Doing CRz({param:.2f}) on q{control} (C) and q{target} (T)")
                qc.crz(param, control, target)
            else:
                raise RuntimeError("Applying control-Rz requires a parameter")
        case "op_ry":
            if param is not None:
                #print(f"Doing CRy({param:.2f}) on q{control} (C) and q{target} (T)")
                qc.cry(param, control, target)
            else:
                raise RuntimeError("Applying control-Ry requires a parameter")
        case _:
            raise RuntimeError(f"Target operation '{op}' not recognised for controlled gate")


    return state.evolve(qc)

def draw_angle_helper(screen, angle):
    """Draw a protector on the screen to help with the angle
    """

    col = (100,100,100)

    line_length = 100

    screen_centre = Vector2(screen.get_width() / 2, screen.get_height() / 2)

    # Line through mouse pointer
    mouse_disp = pygame.mouse.get_pos() - screen_centre
    angle_line = line_length * mouse_disp / mouse_disp.magnitude()
    pygame.draw.line(screen, col, screen_centre, screen_centre + angle_line, 2)

    x_axis = Vector2(line_length, 0)
    pygame.draw.line(screen, col, screen_centre, screen_centre + x_axis, 2)

    # Draw angle arc
    s = 20
    angle_rect = pygame.Rect(screen_centre + Vector2(-s,-s), (2*s,2*s))
    if angle > 0:
        pygame.draw.arc(screen, col, angle_rect, 0, angle)
    else:
        pygame.draw.arc(screen, col, angle_rect, angle, 0)

@dataclass
class QubitNeighbours:
    """Stores location of qubit and surrounding qubit indices
    """
    above: int
    below: int
    left: int
    right: int

def make_qubit_neighbours() -> list[QubitNeighbours]:
    """Create the map of qubit locations

    The layout of qubits is as follows:

                      0    1    2

                   3    4     5     6

                7    8     9     10    11

                  12    13    14    15

                     16    17    18
    """
    return [
        QubitNeighbours(above=0, below=3, left=0, right=1),
        QubitNeighbours(above=1, below=4, left=0, right=2),
        QubitNeighbours(above=2, below=5, left=1, right=2),
        QubitNeighbours(above=0, below=7, left=3, right=4),
        QubitNeighbours(above=1, below=8, left=3, right=5),
        QubitNeighbours(above=2, below=9, left=4, right=6),
        QubitNeighbours(above=6, below=10, left=5, right=6),
        QubitNeighbours(above=3, below=7, left=7, right=8),
        QubitNeighbours(above=4, below=12, left=7, right=9),
        QubitNeighbours(above=5, below=13, left=8, right=10),
        QubitNeighbours(above=6, below=14, left=9, right=11),
        QubitNeighbours(above=11, below=15, left=10, right=11),
        QubitNeighbours(above=8, below=12, left=12, right=13),
        QubitNeighbours(above=9, below=16, left=12, right=14),
        QubitNeighbours(above=10, below=17, left=13, right=15),
        QubitNeighbours(above=11, below=18, left=14, right=15),
        QubitNeighbours(above=13, below=16, left=16, right=17),
        QubitNeighbours(above=14, below=17, left=16, right=18),
        QubitNeighbours(above=15, below=18, left=17, right=18),
    ]

def displace_coords(coords: list[Vector2], offset: Vector2) -> list[Vector2]:
    return [xy + offset for xy in coords]

def make_qubit_locations(config: geometry.LayoutConfig) -> list[Vector2]:
    """Calculate the location of each qubit

    The order of indices matches the state vector index and
    the neighbours index.

    The order of LEDs coordinates returned by the PCB geometry is
    as follows, on the bottom-left board:

        0  ---->  1  ---->  2
                               \
                                \
                                 \
             5  <----   4  <----  3
           /
          /
         /
        6  ---->  7  ---->  8



    """

    board_diagonal = config.board_diagonal()
    tiling_diagonal = config.tiling_diagonal()

    # Offset of the other PCBs compared to the bottom left
    tiling_x, tiling_y = tiling_diagonal

    # Get the basic LED coordinates
    led_coords = config.get_led_coords()

    # Map the coordinates to the four boards
    led_coords_bottom_left = displace_coords(led_coords, Vector2(0, tiling_y))
    led_coords_bottom_right = displace_coords(led_coords, tiling_diagonal)
    led_coords_top_left = geometry.rotate_coords(led_coords, board_diagonal)
    led_coords_top_right = displace_coords(led_coords_top_left, Vector2(tiling_x, 0))
    # Must match order of qubits in the state vector
    qubit_coords = [
        # LED row 0
        led_coords_top_left[6],
        led_coords_top_right[8],
        led_coords_top_right[7],
        # LED row 1
        led_coords_top_left[5],
        led_coords_top_right[3],
        led_coords_top_right[4],
        led_coords_top_right[5],
        # LED row 2
        led_coords_top_left[1],
        led_coords_top_left[0],
        led_coords_top_right[2],
        led_coords_top_right[1],
        led_coords_top_right[0],
        # LED row 3
        led_coords_bottom_left[2],
        led_coords_bottom_right[0],
        led_coords_bottom_right[1],
        led_coords_bottom_right[2],
        # LED row 4
        led_coords_bottom_left[3],
        led_coords_bottom_right[5],
        led_coords_bottom_right[4],
    ]
    return qubit_coords

@dataclass
class RhoQuantumState:
    config: geometry.LayoutConfig = field(init=False)
    qubit_neighbours: list[QubitNeighbours] = field(init=False)
    qubit_locs: list[Vector2] = field(init=False)
    calculate_led_state: Callable[[Statevector], list]
    state: Statevector = field(init=False)
    led_states: list = field(init=False)
    on_states: list[bool] = field(init=False)
    side_length: int = 4
    continuous_gate_in_progress: str | None = None
    continuous_gate_angle: float = 0.0
    control_in_progress: bool = False
    target: int = 0
    control: int = 0
    blink: BlinkControl = field(default_factory=lambda:BlinkControl())

    def __post_init__(self):
        self.state = make_all_zero_state(self.num_qubits())
        self.update_all_leds()
        self.on_states: list[bool] = [True] * self.num_qubits()
        self.config = geometry.LayoutConfig().scale_by(3)
        self.qubit_neighbours = make_qubit_neighbours()
        self.qubit_locs = make_qubit_locations(self.config)

    def update_all_leds(self):
        self.led_states = [
            self.calculate_led_state(self.state, target)
            for target in range(self.num_qubits())
        ]

    def update_led(self, target: int):
        self.led_states[target] = (
            self.calculate_led_state(self.state, target)
        )

    def update(self, screen, rho_quantum_input: RhoQuantumInput):

        command = rho_quantum_input.command
        angle = rho_quantum_input.angle

        if command != "none":
            print(rho_quantum_input)

        if command == "reset":
            self.reset()

        self.handle_movement(command)

        # Set the on/off state of the blinking LEDs. Only blinks the
        # control-qubit if a control gate is in progress.
        self.update_on_states()

        # Record that a controlled operation is in progress
        self.handle_toggle_ctrl(command)

        # Discrete one-qubit operations (gates/measurement)
        if "op" in command:
            if self.control_in_progress:
                self.state = do_controlled_op(
                    self.state,
                    command,
                    self.control,
                    self.target,
                    angle
                )

                self.update_led(self.control)
                self.update_led(self.target)
                self.control_in_progress = False
                self.blink.set_fast(False)
            else:
                self.state = do_one_qubit_op(
                    self.state,
                    command,
                    self.target,
                    angle
                )
                self.update_led(self.target)
                if command == "op_measure":
                    self.update_all_leds()

        if self.continuous_gate_in_progress is not None:
            draw_angle_helper(screen, angle)
            correction = angle - self.continuous_gate_angle
            op = "op_" + self.continuous_gate_in_progress
            if self.control_in_progress:
                self.state = do_controlled_op(self.state,
                                         op,
                                         self.control,
                                         self.target,
                                         correction)
            else:
                self.state = do_one_qubit_op(
                    self.state,
                    op,
                    self.target,
                    correction
                )
                self.update_led(self.target)
            self.continuous_gate_angle = angle

        # Start a continuous one-qubit gate
        if ("_ry" in command) or ("_rz" in command):
            action, gate = command.split("_")
            op = "op_" + gate
            if action == "start":
                self.continuous_gate_in_progress = gate
                self.continuous_gate_angle = 0.0
                if self.control_in_progress:
                    self.state = do_controlled_op(
                        self.state,
                        op,
                        self.control,
                        self.target,
                        0.0
                    )
                else:
                    self.state = do_one_qubit_op(
                        self.state,
                        op,
                        self.target,
                        0.0
                    )
                self.update_led(self.target)
            elif action == "confirm":
                #print_state(state, led_states)
                self.continuous_gate_in_progress = None
                self.control_in_progress = False
                self.blink.set_fast(False)

    def handle_movement(self, command: str):
        """Handle movement commands (arrow keys)

        Returns:
            The new selected qubit (xy-coordinates), or unmodified
                coordinates if no movement command.
        """

        if command == "move_down":
            self.target = self.qubit_neighbours[self.target].below
        if command == "move_up":
            self.target = self.qubit_neighbours[self.target].above
        if command == "move_left":
            self.target = self.qubit_neighbours[self.target].left
        if command == "move_right":
            self.target = self.qubit_neighbours[self.target].right

        if "move" in command:
            # Reset blink counter so that the newly selected
            # LED is briefly off. This is important for
            # responsiveness.
            self.blink.set_off()


    def handle_toggle_ctrl(self, command: str):
        """Enable or disable controlled op if command is toggle_ctrl
        """
        if command == "toggle_ctrl":
            self.control_in_progress = not self.control_in_progress
            self.blink.set_fast(self.control_in_progress)
            if self.control_in_progress:
                self.control = self.target

    def update_on_states(self):
        """Update which LEDs should be flashing
        """
        self.on_states = [True] * self.num_qubits()
        self.on_states[self.target] = self.blink.led_on


        if self.control_in_progress:
            self.on_states[self.control] = self.blink.led_on
        elif self.control != self.target:
            self.on_states[self.control] = True

    def num_qubits(self):
        return 19

    def reset(self):
        self.state = make_all_zero_state(self.num_qubits())
        self.control_gate_in_progress = False
        self.target = 0
        self.control = 0
        self.continuous_gate_in_progress = None
        self.update_led_states()

    def draw(
        self,
        screen,
        width: float,
        height: float,
    ):
        """Draw the LED state for RhoQuantum

        Each qubit has two RGB LEDs, corresponding to the |0) and |1)
            states.

        Qubits are indexed from left-to-right from top-to-bottom
            (i.e. game graphics conventions).

        In this version of RhoQuantum LEDs, the magnitude
        """

        tiling_diag = self.config.tiling_diagonal()
        screen_diag = Vector2(screen.get_width(), screen.get_height())
        pad = 1.2 * (screen_diag - 2*tiling_diag) / 2

        # This is the fixed background image, based on the
        # geometry of the PCBs
        draw.draw_boards(screen, self.config, pad)

        # Get the location of the populated LEDs which represent
        # qubits
        for qubit_index in range(19):
            xy = pad + self.qubit_locs[qubit_index]
            if self.on_states[qubit_index]:
                self.led_states[qubit_index].draw(screen, xy)

        # Get the location of the populated LEDs which represent
        # qubits
        for qubit_index in range(19):
            xy = pad + self.qubit_locs[qubit_index]
            if self.on_states[qubit_index]:
                self.led_states[qubit_index].draw(screen, xy)
        
