"""Keyboard/mouse input for RhoQuantum
"""

import numpy as np
import pygame
from pygame import Vector2
from pygame.joystick import Joystick
from rhoquantum.emulate.inputs import RhoQuantumInput
from dataclasses import dataclass

@dataclass
class UserInput:
    """RhoQuantum Game Controller Interface

    This function reads game controller inputs from pygame and converts it
    into the standard set of RhoQuantum commands. A summary of the
    behaviour of the controls is below:

    * Use the left stick to move qubits

    * 

    """

    joystick: Joystick
    moving: bool = False
    
    
    def __init__(self):
        joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
        if len(joysticks) == 0:
            raise RuntimeError("You must plug a game controller in to use this UserInput")
        elif len(joysticks) > 1:
            raise RuntimeError(f"Too many game controllers plugged in: {len(joysticks)}")
        else:
            self.joystick = joysticks[0]

    def read(self) -> (bool, RhoQuantumInput):
        """Read commands from the game controller

        The first item in the return value is a flag for whether to
        quit the game window. The second parameter is the
        RhoQuantumInput command for controlling the simulation.
        
        Returns:
            A pair (quit_sim, rho_quantum_input).

        """

        rho_quantum_input = RhoQuantumInput()
        quit_sim = False

        # To avoid multiple movement, require the left stick to
        # return to zero before next move
        x = self.joystick.get_axis(0)
        y = self.joystick.get_axis(1)
        if self.moving and (x**2 + y**2) < 0.2:
            self.moving = False
            
            
        
        # Continuously map the angle parameters (they
        # are ignored if not used). You best hope these hardcoded
        # parameters are right! (TODO fix it)
        x = self.joystick.get_axis(2)
        y = self.joystick.get_axis(3) # Positive downwards
        rho_quantum_input.angle = np.arctan2(-y, x)

        # The negative sign is needed because the positive y-axis
        # is downwards, but we still want the angle to be in the
        # normal sense.
        #rho_quantum_input.angle = - 2*np.pi*angle / 360

        # Button mappings for simple PC controller
        button_map = {
            # Right buttons
            1: "right_buttons_bottom",
            2: "right_buttons_right",
            3: "right_buttons_top",
            0: "right_buttons_left",

            # Left/right shoulder
            5: "right_shoulder_top",
            7: "right_shoulder_bottom",
            4: "left_shoulder_top",
            6: "left_shoulder_bottom",  
        }
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_sim = True
            elif event.type == pygame.KEYDOWN:

                # Other ways to quit
                if event.key == pygame.K_q:
                    quit_sim = True
                elif event.key == pygame.K_ESCAPE:
                    quit_sim = True
                    
            elif event.type == pygame.JOYBUTTONDOWN:

                try:
                    pressed = button_map[event.button]

                    match pressed:
                        case "right_buttons_bottom":
                            rho_quantum_input.command = "op_x"
                        case "right_buttons_right":
                            rho_quantum_input.command = "op_h"
                        case "right_buttons_top":
                            rho_quantum_input.command = "op_measure"
                        case "right_buttons_left":
                            rho_quantum_input.command = "toggle_ctrl"
                        case "left_shoulder_top":
                            rho_quantum_input.command = "start_ry"
                        case "left_shoulder_bottom":
                            rho_quantum_input.command = "start_rz"
                        case _:
                            print(f"Unused button {pressed}")
                        
                except KeyError:
                    print(f"Got unrecognised button {event.button}")
                    

            elif event.type == pygame.JOYBUTTONUP:
                
                try:
                    released = button_map[event.button]

                    match released:
                        case "left_shoulder_top":
                            rho_quantum_input.command = "confirm_ry"
                        case "left_shoulder_bottom":
                            rho_quantum_input.command = "confirm_rz"
                        case _:
                            print(f"Unused button {released}")
                        
                except KeyError:
                    print(f"Got unrecognised button {event.button}")
                    
            elif event.type == pygame.JOYAXISMOTION:
                if not self.moving:
                    if self.joystick.get_axis(1) < -0.5:
                        rho_quantum_input.command = "move_up"
                        self.moving = True
                    elif self.joystick.get_axis(1) > 0.5:
                        rho_quantum_input.command = "move_down"
                        self.moving = True
                    elif self.joystick.get_axis(0) < -0.5:
                        rho_quantum_input.command = "move_left"
                        self.moving = True
                    elif self.joystick.get_axis(0) > 0.5:
                        rho_quantum_input.command = "move_right"
                        self.moving = True

        return quit_sim, rho_quantum_input
