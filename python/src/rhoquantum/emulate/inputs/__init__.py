from dataclasses import dataclass

@dataclass
class RhoQuantumInput:
    """Main user-input interface

    This is designed to abstract away the exact input method
    (e.g. keyboard or controller) and provide just the commands
    that the game loop is expecting to handle.

    Here, the commands are implemented as strings, but this can be
    replaced with a more efficient method using integers in the
    implementation.

    The command list is as follows:

    * none: used to indicate that no command was received

    * reset: reset the state to the all-zero state

    * undo: undo applied gate operations. Cannot undo measurement.
    
    * move_up, move_down, move_left, move_right: change the selected
      qubit

    * op_x, op_h: apply a discrete one-qubit gate to the selected
      qubit

    * op_measure: measure the currently selected qubit in the
      computational basis.
    
    * start_ry, start_rz: initiate the application of an Ry or Rz
      gate. Once one of these gates has started, the angle comes from
      the continuous angle_* parameter. While the angle_* parameter is
      being continuously modified, the net effect of the gate is
      continuously updated on the LEDs to show the user what is
      happening. The gate must then be confirmed or cancelled.

    * confirm_ry, confirm_rz: confirm the application of a continuous
      gate. The value of the angle parameter will be guaranteed to be
      valid on receiving this command, and represents the gate angle
      that should be applied.

    * toggle_ctrl: begin the application of a controlled two-qubit
      gate, using the currently selected qubit as control. The user
      can then navigate to the target qubit, and use any one-qubit
      gate to define the controlled operation (including continuous
      gates). The act of performing this one-qubit gate confirms the
      controlled operation. Sending this command a second time cancels
      the current controlled operation.

    * toggle_cycle: start/stop the state vector cycling mode (shows
      non-zero amplitudes).
    
    In addition, the interface provides the following attributes whose
    state can be read at any time (although they are only valid in the
    correct context):

    * angle: the angle of the currently-being-applied continuous gate.

    """

    command: str = "none"
    angle: float = 0.0
