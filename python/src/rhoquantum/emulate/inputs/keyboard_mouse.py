"""Keyboard/mouse input for RhoQuantum
"""

import numpy as np
import pygame
from dataclasses import dataclass
from pygame import Vector2
from rhoquantum.emulate.inputs import RhoQuantumInput

@dataclass
class UserInput:
    """RhoQuantum Keyboard/Mouse Interface

    This function reads keyboard/mouse input from pygame and converts it
    into the standard set of RhoQuantum commands. A summary of the
    behaviour of the keyboard/mouse is as follows:

    * The arrow keys are used to move around between different qubits.

    * A discrete one-qubit gate is applied on a down-press of the x
      and h keys.

    * A down-press of m measures the currently selected qubit.
    
    * Continuous one-qubit gates are applied by holding down y or z
      and setting the angle using the mouse. To set the angle, press
      and hold the left mouse button (current pointer location defines
      the centre of a circle on the screen). Then, move the mouse out
      from the point and around the imaginary circle to define an
      angle. To apply the gate, release y/z while the left mouse
      button is still pressed. To cancel the gate, release the left
      mouse button before releasing y/z.
    
    * To start a controlled operation, press c. Press c again to
      cancel it.

    * To cycle through the state vector, press s. Press s again to
      switch back to normal mode.

    """

    screen_centre: Vector2

    def __init__(self, screen):
        self.screen_centre = Vector2(screen.get_width() / 2, screen.get_height() / 2)
    
    def read(self) -> (bool, RhoQuantumInput):
        """Read commands from the keyboard

        The first item in the return value is a flag for whether to
        quit the game window. The second parameter is the
        RhoQuantumInput command for controlling the simulation.
        
        Returns:
            A pair (quit_sim, rho_quantum_input).

        """

        rho_quantum_input = RhoQuantumInput()
        quit_sim = False

        # Continuously map the angle parameters (they
        # are ignored if not used)
        xy = pygame.mouse.get_pos()
        current_mouse_pos = Vector2(xy[0], xy[1])
        mouse_displacement = current_mouse_pos - self.screen_centre
        positive_x_axis = Vector2(1,0)
        angle = positive_x_axis.angle_to(mouse_displacement)

        # The negative sign is needed because the positive y-axis
        # is downwards, but we still want the angle to be in the
        # normal sense.
        rho_quantum_input.angle = - 2*np.pi*angle / 360
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_sim = True
            elif event.type == pygame.KEYDOWN:

                # Other ways to quit
                if event.key == pygame.K_q:
                    quit_sim = True
                elif event.key == pygame.K_ESCAPE:
                    quit_sim = True

                # Reset/undo
                elif event.key == pygame.K_r:
                    rho_quantum_input.command = "reset"
                elif event.key == pygame.K_u:
                    rho_quantum_input.command = "undo"

                # Movement keys
                elif event.key == pygame.K_UP:
                    rho_quantum_input.command = "move_up"
                elif event.key == pygame.K_DOWN:
                    rho_quantum_input.command = "move_down"
                elif event.key == pygame.K_RIGHT:
                    rho_quantum_input.command = "move_right"
                elif event.key == pygame.K_LEFT:
                    rho_quantum_input.command = "move_left"

                # One-qubit gates
                elif event.key == pygame.K_x:
                    rho_quantum_input.command = "op_x"
                elif event.key == pygame.K_h:
                    rho_quantum_input.command = "op_h"

                # One-qubit measurement
                elif event.key == pygame.K_m:
                    rho_quantum_input.command = "op_measure"

                # Start a controlled operation. Navigate
                # to target qubit, then press a one-qubit
                # gate key (e.g. press X to perform CNOT).
                # Pressing c again cancels the operation.
                elif event.key == pygame.K_c:
                    rho_quantum_input.command = "toggle_ctrl"


                elif event.key == pygame.K_s:
                    rho_quantum_input.command = "toggle_cycle"
                    
                # Start a continuous gate (down-press y/z)
                elif event.key == pygame.K_y:
                    rho_quantum_input.command = "start_ry"
                elif event.key == pygame.K_z:
                    rho_quantum_input.command = "start_rz"

            elif event.type == pygame.KEYUP:

                # Confirm or cancel a continuous gate
                if event.key == pygame.K_y:
                    rho_quantum_input.command = "confirm_ry"
                elif event.key == pygame.K_z:
                    rho_quantum_input.command = "confirm_rz"

        return quit_sim, rho_quantum_input
