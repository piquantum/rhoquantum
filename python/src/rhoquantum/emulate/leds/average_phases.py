"""An attempt to average the global phase on each |0)/|1) component.

The intention of this (2-RGB LED) representation was to make it
possible to see the effect of Rz(theta) on the |1) state, by averaging the |1) and |0) (global) phases separately. It does not work well, because the phase of every LED changes on single-qubit operations.
"""

import numpy as np
import pygame
from dataclasses import dataclass
from pygame import Vector2
from rhoquantum.common import make_led_colour, map_angle
from qiskit.quantum_info import Statevector

@dataclass
class LedState:
    M_0: float = 0
    M_1: float = 0
    phase_0: float = 0
    phase_1: float = 0

    def draw(self, screen, pos):
        """Draw the qubit to the screen

        In this version, the magnitude of the |0) and |1) components
        set the intensities of the LEDs.

        The colours of the LEDs are set based on the "phases" for |0)
        and |1), which are averages of the arguments of the
        corresponding set of amplitudes in the state vector (i.e. no
        phase differences are computed). This means the colours behave
        independently.
        """

        M_0 = min(self.M_0, 1.0)
        M_1 = min(self.M_1, 1.0)

        # Offset down to |0) and up to |1)
        offset = Vector2(0, 15)

        col_0 = make_led_colour(M_0, map_angle(self.phase_0))
        col_1 = make_led_colour(M_1, map_angle(self.phase_1))

        # Draw |0) and |1) LEDs
        pygame.draw.circle(screen, col_0, pos - offset, 10)
        pygame.draw.circle(screen, col_1, pos + offset, 10)

def calculate_led_states(state: Statevector) -> list[LedState]:
    """Average the phases in the state vector

    This algorithm (involving one pass over the state vector) can be
    used to update all the LED states in place.
    """

    led_states = []
    for _ in range(state.num_qubits):
        led_states.append(LedState())

    for index, amplitude in enumerate(state):
        for n in range(state.num_qubits):
            bit_n = 1 & (index >> n)
            mag = np.abs(amplitude)**2
            if bit_n == 0:
                led_states[n].M_0 += mag
                led_states[n].phase_0 += mag * np.angle(amplitude)
            else:
                led_states[n].M_1 += mag
                led_states[n].phase_1 += mag * np.angle(amplitude)


    return led_states

