"""Representing the mixed state of the qubit on two LEDs
"""

import pygame
import numpy as np
from pygame import Vector2
from dataclasses import dataclass
from qiskit.quantum_info import DensityMatrix, Statevector
from rhoquantum.common import one_qubit_mixed_state, make_led_colour, map_angle

import rhoquantum.emulate.sharedlib as lib

epsilon = 1e-4

@dataclass
class LedState:
    M_0: float # Probability of measuring 0
    M_1: float # Probability of measuring 1
    phase: float # Average phase of mixed state

    def __init__(self, rho: DensityMatrix = None, M_0 = None, M_1 = None, phase = None):
        """Construct an LED state from the one-qubit density matrix

        The one-qubit mixed state contains the probabilities of
        measuring |0) or |1) on the diagonal, and the off-diagonal
        contains an angle which may be used as a phase.

        When the probability of measuring |0) or |1) is 1, the
        phase is defined to be zero.
        """
        if rho is None:
            self.M_0 = M_0
            self.M_1 = M_1
            self.phase = phase
            return

        # Use the diagonal elements of the mixed state
        # as the brightness of the LEDs (expecting real)
        matrix = rho.data
        self.M_0 = np.real(matrix[0,0])
        self.M_1 = np.real(matrix[1,1])

        # Use the argument of the [1,0] element as the phase.
        # However, if the state is |0) or |1), then the phase is
        # always defined to be zero (because any phase is global).
        # In practice, this ignores the numerically-unstable phase
        # on the off diagonal in this case.
        if (
                (np.abs(self.M_0) < epsilon) or
                (np.abs(self.M_1) < epsilon) or
                (np.abs(matrix[1,0]) < epsilon)
        ):
            self.phase = 0
        else:
            self.phase = map_angle(np.angle(matrix[1,0]))

    def draw(self, screen, pos):
        """Draw the qubit to the screen

        In this version, the magnitude of the |0) and |1) components
        set the intensities of the LEDs.

        The phase difference (taken from the argument of the lower
        off-diagonal element of the state vector) is used to set the
        colour-wheel position of both LEDs, in opposite directions.

        """

        M_0 = min(self.M_0, 1.0)
        M_1 = min(self.M_1, 1.0)

        # Offset down to |0) and up to |1)
        offset = Vector2(7, 0)

        # Convert the LED state to colours (you can swap this
        # out for a different function)
        col_0 = make_led_colour(M_0, map_angle(self.phase))
        col_1 = make_led_colour(M_1, map_angle(-self.phase))

        # Draw |0) and |1) LEDs
        pygame.draw.circle(screen, col_0, pos - offset, 7)
        pygame.draw.circle(screen, col_1, pos + offset, 7)


def calculate_led_state(state: Statevector, target: int) -> LedState:
    """Calculate the mixed state of the qubit represented using LEDs

    The steps are:

    1. Calculate the mixed state
    2. Use the elements of the density matrix to get the magnitude
       and phase for the LED
    """

    # rho = one_qubit_mixed_state(state, target)
    # return LedState(rho)
    lib.printQubit(lib.s)
    return LedState(M_0 = lib.s.contents.m[target][0],
                    M_1 = lib.s.contents.m[target][1],
                    phase = lib.s.contents.phi[target])
