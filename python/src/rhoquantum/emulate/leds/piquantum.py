"""This is the representation used in PiQuantum

The representation used a single RGB LED per qubit. The probabilities
of measuring |0) and |1) set the brightness of the red/blue
components, and the intensity of the phase colour (green) is set to
the average of the non-zero phase differences between all the
|1)-|0)-amplitude pairs in the state vector for the qubit.
"""

