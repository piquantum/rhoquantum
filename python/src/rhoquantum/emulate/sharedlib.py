from ctypes import cdll, POINTER, c_uint8, c_int16, c_int32, c_float, Structure

rq = cdll.LoadLibrary("/home/oli/git/rhoquantum/src/librhoquantum.so")

# "Typedef"
Matrix2 = (c_int32 * 8)

class State(Structure):
    """Struct corresponding to State in rho quantum.h
    """
    _fields_ = [
        ('v', c_int16 * 1048576),
	    ('m', (c_float * 2) * 19),
        ('phi', c_float * 19),
        ('colours', (c_uint8 * 3) * 19)
    ]

# Import functions
makeState = rq.makeState
makeState.restype = POINTER(State)

singleQubit = rq.singleQubit
singleQubit.argtypes = (POINTER(State), Matrix2, c_uint8)

twoQubit = rq.twoQubit
twoQubit.argtypes = (POINTER(State), Matrix2, c_uint8, c_uint8)

setRx = rq.setRx
setRx.argtypes = (c_float,)
setRy = rq.setRy
setRy.argtypes = (c_float,)
setRz = rq.setRz
setRz.argtypes = (c_float,)

printv = rq.printv
printv.argtypes = (POINTER(State),)

printm = rq.printm
printm.argtypes = (Matrix2,)

printQubit = rq.printQubit
printQubit.argtypes = (POINTER(State),)

# Add single qubit gates
X = Matrix2.in_dll(rq, "X")
Y = Matrix2.in_dll(rq, "Y")
Z = Matrix2.in_dll(rq, "Z")
H = Matrix2.in_dll(rq, "H")
T = Matrix2.in_dll(rq, "T")
Rx = Matrix2.in_dll(rq, "Rx")
Ry = Matrix2.in_dll(rq, "Ry")
Rz = Matrix2.in_dll(rq, "Rz")

s = makeState()

# example Main
if __name__ == "__main__":

    singleQubit(s, X, 0)
    # rotation gates have to be set before they can be used
    setRy(0.25)
    singleQubit(s, Ry, 4)

    setRx(0.4)
    singleQubit(s, Rx, 6)

    singleQubit(s, T, 4)
    singleQubit(s, H, 5)

    twoQubit(s, X, 5, 10)

    printQubit(s)

    # This is where you get the colours
    for qubit_num, qubit_colour_array in enumerate(s.contents.colours):
        print(f"Qubit {qubit_num}: ", end="")
        print(f"RGB: [", end="")
        for colour in qubit_colour_array:
            print(f"{colour},", end="")
        print("]")
