"""Draw the RhoQuantum board for the emulator
"""

from pygame import draw, transform, Surface, Vector2, Rect
from rhoquantum.hardware import geometry

def make_leds(config: geometry.LayoutConfig, fill: bool) -> Surface:
    """A qubit consists of two LEDs
    """

    led_width, led_height = config.led_diagonal
    
    qubit_led_area = Surface((led_width, 2 * led_height))

    led_0 = Rect(0, 0, led_width, led_height)
    led_1 = Rect(0, led_height, led_width, led_height)

    draw.rect(qubit_led_area, "red", led_0)
    draw.rect(qubit_led_area, "blue", led_1)

    if not fill:
        border = 1
        inner_0 = Rect(
            border,
            border,
            led_width - 2 * border,
            led_height - 2 * border,
        )
        inner_1 = Rect(
            border,
            led_height + border,
            led_width - 2 * border,
            led_height - 2 * border,
        )
        draw.rect(qubit_led_area, "lightgreen", inner_0)
        draw.rect(qubit_led_area, "lightgreen", inner_1)

    return transform.rotate(qubit_led_area, 90)

def make_screw(config: geometry.LayoutConfig) -> Surface:
    """Make a screw hole and pad"""
    diam = config.screw_hole_pad_diam
    screw_surface = Surface((diam, diam))
    screw_surface.fill("lightgreen")

    pad_radius = diam / 2
    draw.circle(screw_surface, "gold", (diam / 2, diam / 2), pad_radius)

    hole_radius = config.screw_hole_diam / 2
    draw.circle(screw_surface, "white", (diam / 2, diam / 2), hole_radius)

    return screw_surface

def correct_top_left(xy: Vector2, surface: Surface) -> Vector2:
    """Correct the coordinates to point to the top left corner

    If you want to place a surface centred at xy, call this
    function to correct the coordinates to point to the top left,
    so that it is located correctly when placed using pygame.
    """
    return xy - Vector2(surface.get_width(), surface.get_height()) / 2

def make_single_pcb(config: geometry.LayoutConfig) -> Surface:
    """Draw a single PCB, ready for tiling
    """
    screw_coords = config.get_screw_coords()
    led_coords = config.get_led_coords()

    # Draw the board
    pcb_surface = Surface(config.board_diagonal())
    pcb_surface.fill("lightgreen")

    # Draw the screws
    for xy in screw_coords:
        screw_surface = make_screw(config)
        pcb_surface.blit(screw_surface, correct_top_left(xy, screw_surface))

    # Draw the LEDs
    for xy in led_coords:
        led_surface = make_leds(config, False)
        pcb_surface.blit(led_surface, correct_top_left(xy, led_surface))
        
    return pcb_surface

def draw_boards(screen: Surface, config: geometry.LayoutConfig,
                top_left_corner: Vector2):
    """Draw the full RhoQuantum boards
    """
    
    pcb_surface_bottom_left = make_single_pcb(config)
    pcb_surface_bottom_right = make_single_pcb(config)
    pcb_surface_top_left = transform.rotate(make_single_pcb(config), 180)
    pcb_surface_top_right = transform.rotate(make_single_pcb(config), 180)

    tiling_diag = config.tiling_diagonal()

    bottom_left =  top_left_corner + Vector2(0,tiling_diag[1])
    bottom_right = bottom_left + Vector2(tiling_diag[0], 0)
    top_left = bottom_left + Vector2(0, -tiling_diag[1])
    top_right = bottom_left + Vector2(tiling_diag[0], -tiling_diag[1])

    # Draw the PCB behind slightly darker
    middle = (bottom_left + top_right) / 2
    pcb_surface_behind = Surface(config.board_diagonal())
    pcb_surface_behind.fill("darkgreen")
    screen.blit(pcb_surface_behind, middle)

    screen.blit(pcb_surface_bottom_left, bottom_left)
    screen.blit(pcb_surface_bottom_right, bottom_right)
    screen.blit(pcb_surface_top_left, top_left)
    screen.blit(pcb_surface_top_right, top_right)

