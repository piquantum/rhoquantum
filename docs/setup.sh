#!/usr/bin/env sh

######################################################################
# @author      : oli (oli@$HOSTNAME)
# @file        : setup
# @created     : Thursday Feb 29, 2024 22:46:09 GMT
#
# @description : 
######################################################################

# remove all the node junk
sudo rm ${which antora}
rm ./package-lock.json
rm ./package.json
rm -rf ./node_modules

# start again
npm install gulp
npm install gulp-connect
npm install antora
npm install @antora/site-generator-default
npm install browser-sync

# it works!
make serve

# you can run antora with
# npx antora --fetch antora-playbook.yml

