= Neopixel Constraints
:stem: latexmath

A simple option for the qubit LEDs is to use Neopixels. Neopixels are RGB LEDs
with an embedded controller, which manages a PWM that allows each LED to be
settable with a 24-bit colour code.

Neopixels can be cascaded in a chain and controlled using a single wire
protocol (WS2812B). The Neopixels in the chain pass data from one to the next,
like a shift register, meaning only a single digital line is required to drive
a large number of LEDs.

The Neopixels retain their RGB state until another set of RGB values is sent,
keeping the CPU overhead required to drive the LEDs to a minimum. This is a
substantial improvement compared to PiQuantum, which used software-generated
PWM to controller "regular" RGB LEDs (i.e. on/off only).

The protocol used by the Neopixels is not a simple standard like SPI or I2C,
and normally requires some kind of trickery (e.g. misusing a PWM or USART
periperhal, or configurable logic) to realise the correct waveform. This is
microcontroller-specific.

This file contains some details about the time required to update the Neopixels
(as a function of the number of LEDs required), and how to drive the Neopixels
using the various microcontrollers under consideration.

== Neopixel Timing

For details on the protocol required to drive Neopixels, consult the WS2812B
datasheet. In summary, the timing of the protocol is as follows

* 1.25 us required for each bit
* 24 bits required to transmit a single LED's RGB value
* `N` RGB values required to be sent serially (one for each of `N` RGB LEDs)
* Fixed 50 us delay to latch the data into the RGB LEDs

If all the LEDs are connected to the same line, then the time required to
update the LEDs is stem:[(30N + 50)] us. The following table shows the time
required to update all the qubit LEDs for various numbers of qubits and
LEDs-per-qubit.

.Time required to refresh LEDs based on number of qubits and LEDs-per-qubit
[cols="1,1,1,1"]
|===
|    | 1 LED/qubit | 2 LEDs/qubit | 3 LEDs/qubit

|16  | 530 us      | 1.01 ms      | 1.49 ms

|18  | 590 us      | 1.13 ms      | 1.67 ms

|20  | 650 us      | 1.25 ms      | 1.85 ms

|22  | 710 us      | 1.37 ms      | 2.03 ms

|24  | 770 us      | 1.49 ms      | 2.21 ms

|26  | 830 us      | 1.61 ms      | 2.39 ms

|===

If all the LEDs are in a single line, it is not possible to change just one LED
without refreshing all of them. Therefore, it is simplest to assume the
worst-case timing for any LED update (even if just one is changing).

The latency can be halved by splitting the Neopixels into two lines, at the
expense of using a second microcontroller peripheral.
