# Script showing stability of one-qubit mixed state
#
# This script shows that the mixed state of a given qubit (referred to
# as the qubit under test below) is invariant under arbitrary quantum
# operations applied to other qubits.
#
# The fact underlies the efficient implementation of the
# state-vector-to-LED mapping algorithm, by showing that the only LEDs
# that need their state updating are those that are involved in the
# gate that has just been applied.
# 
# As a result, it possible to roll the calculation of the updated LED
# state into the single pass of the state vector required to implement
# the gate, meaning this operation has essentially no overhead.
#
# How this script works:
#
# 1. Prepare a random initial state (the assumption is that the
#    qubit-under-test will be entangled with the others in this
#    state).
# 2. Calculate the mixed state of the qubit-under-test by partially
#    tracing out all the others.
# 3. Make a new random circuit that does not involve the
#    qubit-under-test.
# 4. Re-calculate the mixed state of the qubit-under-test, and
#    verify that it is the same as it was before.
#

from qiskit import QuantumCircuit
from qiskit.circuit.random import random_circuit
from qiskit.quantum_info import random_statevector, partial_trace, state_fidelity

num_qubits = 8
dim = 2**num_qubits
depth = 8

# Which qubit to test the mixed state
qubit_under_test = 2

# The list of other qubits (to be partially-traced out)
other_qubits = [n for n in range(num_qubits) if n != qubit_under_test]

# Start in a random state
state = random_statevector(dim)

# Take the partial trace over all but qubit_under_test
pt0 = partial_trace(state, other_qubits)
print("Mixed state of qubit {qubit_under_test} after first random circuit:\n")
print(pt0)

# Now apply a random circuit where none of the gates involve
# qubit_under_test
c1 = QuantumCircuit(num_qubits)
c1_rand = random_circuit(num_qubits - 1, depth)
c1 = c1.compose(c1_rand, qubits=other_qubits)
print(f"\nRandom circuit (does not touch {qubit_under_test}):")
print(c1)

# Apply the second random circuit
state = state.evolve(c1)

# Now get the mixed state of the qubit_under_test again
pt1 = partial_trace(state, other_qubits)
print(f"\nMixed state of qubit {qubit_under_test} after first random circuit:\n")
print(pt1)

# Check if the mixed state of qubit_under_test changed
fidelity = state_fidelity(pt0, pt1)
print(f"\nFidelity between the two mixed states on Q0: {fidelity}")
