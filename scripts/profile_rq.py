# Simple profiling script for rq_emulate

import pygame
from rhoquantum.emulate.inputs import RhoQuantumInput
from rhoquantum.emulate.leds import mixed_state
from rhoquantum.emulate import state

import cProfile

profiler = cProfile.Profile()

screen = pygame.display.set_mode((750, 750))
calculate_led_states = mixed_state.calculate_led_states

profiler.enable()

rho_quantum_state = state.RhoQuantumState(calculate_led_states)
rho_quantum_input = RhoQuantumInput(command="op_x")
rho_quantum_state.update(screen, rho_quantum_input)

profiler.disable()
profiler.print_stats(sort="cumtime")
