# Scripts for Development/Use

The Python scripts in this folder use the `rhoquantum` package. Before running the scripts, create a virtual environment and install the package, as follows (on Linux):

```bash
# Create and activate the environment
python3 -m venv venv
source venv/bin/activate

# Use -e to be able to edit the python package
pip install -e ../rhoquantum 
```

Then, run a script using `python script.py`
