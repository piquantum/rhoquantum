#/bin/bash
# Run the STM32CubeMX peripheral and pin-mapping
# configuration tool (for initialisation code
# generation). Using a script because you cannot
# put this in the path without errors.
$HOME/STM32CubeMX/STM32CubeMX
