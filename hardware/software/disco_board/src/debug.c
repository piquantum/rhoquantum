#include <stm32f7xx.h>

/**
 * \brief Redirect output to  SWO pinout*
 *
 * This will overwrite the newlib _write stub,
 * and redirect output over the serial wire debugging
 * port. Make sure `System Core` -> `Sys` ->
 * `Trace asynchronous Sw` is enabled, and take note
 * of the APB1 timer clock frequency (which is used
 * to configure the debugger).
 *
 * Taken from https://www.codeinsideout.com/blog/stm32/swv/#override-system-calls.
 *
 */
int _write(int file, char *ptr, int len) {
  int DataIdx;
  for (DataIdx = 0; DataIdx < len; DataIdx++) {
      ITM_SendChar(*ptr++);
  }
  return len;
}
