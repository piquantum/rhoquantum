/**
 * \brief Main user application entry point
 *
 * The code here uses CMSIS v2 (based on FreeRTOS) to manage
 * a multi-threaded application. The entry point is user_main,
 * which gets called from the default task (in the "real" main
 * file, which is Core/Src/main.c).
 *
 * Tasks are functions
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "cmsis_os2.h"
#include "main.h"
#include "neopixel.h"
#include "st7735/st7735.h"
#include "stm32f7xx_hal_adc.h"
#include "stm32f7xx_hal_tim_ex.h"

extern ADC_HandleTypeDef hadc1;
extern TIM_HandleTypeDef htim2;

/// Task 1: blink the green LED on and off for every full SDRAM write/read
void blinky_led() {

    int count = 0;
    while (1) {
	HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
	printf("LED Blinked: %d\n", count++);
	osDelay(1000);
    }
}

/// Task 2: cycle through the colour wheel on 8 neopixels
void neopixel_colours() {

    uint32_t degrees = 0;
    uint8_t r, g, b;

    // Turn all the neopixels off
    clear_buffer();
    write_neopixels();


    while(1) {

	float x = 2.0 * 3.14 * degrees / 360.0;

	// Set the brighntess
	float scale = 20.0;

	r = scale * pow(cos(x), 2);
	g = scale * pow(cos(x + 2*3.14/3.0), 2);
	b = scale * pow(cos(x - 2*3.14/3.0), 2);

	for (unsigned i = 0; i < 8; ++i) {
	    set_neopixel(i, r, g, b);
	}

	// Flush the neopixel buffer to the LEDs
	write_neopixels();

	if (degrees == 359) {
	    degrees = 0;
	} else {
	    degrees++;
	}

	osDelay(10);
    }
}

/// Task 3: Check the SPI display works
void display_test() {

    ST7735_Init();

    // Test program from library below...
    while (1) {

	// Check border
	ST7735_FillScreen(ST7735_BLACK);

	for(int x = 0; x < ST7735_WIDTH; x++) {
	    ST7735_DrawPixel(x, 0, ST7735_RED);
	    ST7735_DrawPixel(x, ST7735_HEIGHT-1, ST7735_RED);
	}

	for(int y = 0; y < ST7735_HEIGHT; y++) {
	    ST7735_DrawPixel(0, y, ST7735_RED);
	    ST7735_DrawPixel(ST7735_WIDTH-1, y, ST7735_RED);
	}

	osDelay(3000);

	// Check fonts
	ST7735_FillScreen(ST7735_BLACK);
	ST7735_WriteString(0, 0, "Font_7x10, red on black, lorem ipsum dolor sit amet", Font_7x10, ST7735_RED, ST7735_BLACK);
	ST7735_WriteString(0, 3*10, "Font_11x18, green, lorem ipsum", Font_11x18, ST7735_GREEN, ST7735_BLACK);
	ST7735_WriteString(0, 3*10+3*18, "Font_16x26", Font_16x26, ST7735_BLUE, ST7735_BLACK);
	osDelay(2000);

	// Check colors
	ST7735_FillScreen(ST7735_BLACK);
	ST7735_WriteString(0, 0, "BLACK", Font_11x18, ST7735_WHITE, ST7735_BLACK);
	osDelay(500);

	ST7735_FillScreen(ST7735_BLUE);
	ST7735_WriteString(0, 0, "BLUE", Font_11x18, ST7735_BLACK, ST7735_BLUE);
	osDelay(500);

	ST7735_FillScreen(ST7735_RED);
	ST7735_WriteString(0, 0, "RED", Font_11x18, ST7735_BLACK, ST7735_RED);
	osDelay(500);

	ST7735_FillScreen(ST7735_GREEN);
	ST7735_WriteString(0, 0, "GREEN", Font_11x18, ST7735_BLACK, ST7735_GREEN);
	osDelay(500);

	ST7735_FillScreen(ST7735_CYAN);
	ST7735_WriteString(0, 0, "CYAN", Font_11x18, ST7735_BLACK, ST7735_CYAN);
	osDelay(500);

	ST7735_FillScreen(ST7735_MAGENTA);
	ST7735_WriteString(0, 0, "MAGENTA", Font_11x18, ST7735_BLACK, ST7735_MAGENTA);
	osDelay(500);

	ST7735_FillScreen(ST7735_YELLOW);
	ST7735_WriteString(0, 0, "YELLOW", Font_11x18, ST7735_BLACK, ST7735_YELLOW);
	osDelay(500);

	ST7735_FillScreen(ST7735_WHITE);
	ST7735_WriteString(0, 0, "WHITE", Font_11x18, ST7735_BLACK, ST7735_WHITE);
	osDelay(500);
    }
}

void user_main() {

    // Configure and start task 1 (blinking LED)
    const osThreadAttr_t blinky_led_attr = {
	.name = "blinky_led",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };
    osThreadNew(blinky_led, NULL, &blinky_led_attr);

    // Configure and start task 2 (neopixel colour cycling)
    const osThreadAttr_t neopixel_colours_attr = {
	.name = "neopixel_colours",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };
    osThreadNew(neopixel_colours, NULL, &neopixel_colours_attr);

    // Configure and start task 3 (TFT display test)
    const osThreadAttr_t display_test_attr = {
	.name = "display_test",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };
    osThreadNew(display_test, NULL, &display_test_attr);

    osDelay(osWaitForever);
    while (1)
	;
}
