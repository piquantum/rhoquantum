/**
 * \brief Main user application entry point
 *
 * The code here uses CMSIS v2 (based on FreeRTOS) to manage
 * a multi-threaded application. The entry point is user_main,
 * which gets called from the default task (in the "real" main
 * file, which is Core/Src/main.c).
 *
 * Tasks are functions
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "cmsis_os2.h"
#include "main.h"
#include "neopixel.h"
#include "st7735/st7735.h"
#include "stm32f7xx_hal_adc.h"
#include "stm32f7xx_hal_gpio.h"
#include "stm32f7xx_hal_hcd.h" 
#include "stm32f7xx_hal_rcc.h"
#include "stm32f7xx_hal_tim_ex.h"

#include "usb_host.h"
#include "sdram.h"

extern ADC_HandleTypeDef hadc1;
extern TIM_HandleTypeDef htim3;

/// Flag to enable the SDRAM test (note it is set/cleared in the
/// USB thread which has a tight loop).
bool do_sdram_test = false;

/// Task 1: blink the green LED on and off for every full SDRAM write/read
/// Expect the LED to toggle once every roughly 6 seconds (time to write
/// full 18MiB SDRAM). Press BTN2 to start/stop the test.
void sdram_test() {

    sdram_init_sequence(REFRESH_COUNT);

    uint8_t counter = 0;
    while(1) {

	if (do_sdram_test) {

	    
	    
	    // Write values to the full SDRAM device
	    for (uint32_t offset = 0; offset < SDRAM_DEVICE_SIZE / 16; ++offset) {
		uint32_t address = SDRAM_DEVICE_ADDR + offset;
		volatile uint8_t *sdram_byte = (uint8_t*)address;

		// Write a "random" value
		*sdram_byte = 0xff & (counter*offset + 3);
	    }

	    // Read values from the full SDRAM device
	    bool correct = true;
	    for (uint32_t offset = 0; offset < SDRAM_DEVICE_SIZE / 16; ++offset) {
		uint32_t address = SDRAM_DEVICE_ADDR + offset;
		volatile uint8_t *sdram_byte = (uint8_t*)address;

		// Read the SDRAM and compare with the correct value
		uint8_t correct_value = 0xff & (counter*offset + 3);
		if (*sdram_byte != correct_value) {
		    correct = false;
		}
	    }

	    // Toggle LED for every correct pass
	    if (correct) {
		HAL_GPIO_TogglePin(LD8_GPIO_Port, LD8_Pin);
		printf("SDRAM verification passed");
	    }

	    ++counter;

	}
	
	osDelay(1);
    }
    
}

/**
 * \brief Returns a value between 0.0 and 1.0 for the ADC reading
 *
 * Assuming 12-bit ADC, set a range of voltages that will be mapped
 * to [0.0, 1.0]
 *
 * 
 */
float read_potentiometer(float low_voltage, float high_voltage) {

    HAL_ADC_Start(&hadc1);
    while (HAL_ADC_PollForConversion(&hadc1, 1000) != HAL_OK)
        ; // Wait for ADC to complete

    uint32_t adc_value = HAL_ADC_GetValue(&hadc1);
    HAL_ADC_Stop(&hadc1);

    // VREF+ is 3.3V and VREF- is 0V, so the 12 bits equally
    // divide the range 0-3.3V
    const uint32_t adc_3v3 = 0xfff;

    float voltage = 3.3 * (float)adc_value / (float)adc_3v3;

    if (voltage > high_voltage) {
        return 1.0;
    } else if (voltage < low_voltage) {
        return 0.0;
    } else {
        // Voltage is in range
        return (voltage - low_voltage) / (high_voltage - low_voltage);
    }
}

/// Task 2: cycle through the colour wheel on 8 neopixels
void neopixel_colours() {

    uint32_t degrees = 0;
    uint8_t r, g, b;

    // Turn all the neopixels off
    clear_buffer();
    write_neopixels();

    
    while(1) {

	float x = 2.0 * 3.14 * degrees / 360.0;

	// Scale LED brightness using POT.

	// Voltage range chosen quite small due to using a trim pot,
	// and want to magnify a small change in the reading
	float scale = 20.0 * read_potentiometer(0.1, 1);
        
	r = scale * pow(cos(x), 2);
	g = scale * pow(cos(x + 2*3.14/3.0), 2);
	b = scale * pow(cos(x - 2*3.14/3.0), 2);

	for (unsigned i = 0; i < 38; ++i) {
	    set_neopixel(i, r, g, b);
	}
	
	// Flush the neopixel buffer to the LEDs
	write_neopixels();
	
	if (degrees == 359) {
	    degrees = 0;
	} else {
	    degrees++;
	}

	osDelay(10);
    }
}

/// Task 3: Check the SPI display works
void display_test() {

    ST7735_Init();

    // Test program from library below...
    while (1) {

	// Check border
	ST7735_FillScreen(ST7735_BLACK);

	for(int x = 0; x < ST7735_WIDTH; x++) {
	    ST7735_DrawPixel(x, 0, ST7735_RED);
	    ST7735_DrawPixel(x, ST7735_HEIGHT-1, ST7735_RED);
	}

	for(int y = 0; y < ST7735_HEIGHT; y++) {
	    ST7735_DrawPixel(0, y, ST7735_RED);
	    ST7735_DrawPixel(ST7735_WIDTH-1, y, ST7735_RED);
	}

	osDelay(3000);

	// Check fonts
	ST7735_FillScreen(ST7735_BLACK);
	ST7735_WriteString(0, 0, "Font_7x10, red on black, lorem ipsum dolor sit amet", Font_7x10, ST7735_RED, ST7735_BLACK);
	ST7735_WriteString(0, 3*10, "Font_11x18, green, lorem ipsum", Font_11x18, ST7735_GREEN, ST7735_BLACK);
	ST7735_WriteString(0, 3*10+3*18, "Font_16x26", Font_16x26, ST7735_BLUE, ST7735_BLACK);
	osDelay(2000);

	// Check colors
	ST7735_FillScreen(ST7735_BLACK);
	ST7735_WriteString(0, 0, "BLACK", Font_11x18, ST7735_WHITE, ST7735_BLACK);
	osDelay(500);

	ST7735_FillScreen(ST7735_BLUE);
	ST7735_WriteString(0, 0, "BLUE", Font_11x18, ST7735_BLACK, ST7735_BLUE);
	osDelay(500);

	ST7735_FillScreen(ST7735_RED);
	ST7735_WriteString(0, 0, "RED", Font_11x18, ST7735_BLACK, ST7735_RED);
	osDelay(500);

	ST7735_FillScreen(ST7735_GREEN);
	ST7735_WriteString(0, 0, "GREEN", Font_11x18, ST7735_BLACK, ST7735_GREEN);
	osDelay(500);

	ST7735_FillScreen(ST7735_CYAN);
	ST7735_WriteString(0, 0, "CYAN", Font_11x18, ST7735_BLACK, ST7735_CYAN);
	osDelay(500);

	ST7735_FillScreen(ST7735_MAGENTA);
	ST7735_WriteString(0, 0, "MAGENTA", Font_11x18, ST7735_BLACK, ST7735_MAGENTA);
	osDelay(500);

	ST7735_FillScreen(ST7735_YELLOW);
	ST7735_WriteString(0, 0, "YELLOW", Font_11x18, ST7735_BLACK, ST7735_YELLOW);
	osDelay(500);

	ST7735_FillScreen(ST7735_WHITE);
	ST7735_WriteString(0, 0, "WHITE", Font_11x18, ST7735_BLACK, ST7735_WHITE);
	osDelay(500);

#ifdef ST7735_IS_128X128
	// Display test image 128x128
	ST7735_DrawImage(0, 0, ST7735_WIDTH, ST7735_HEIGHT, (uint16_t*)test_img_128x128);
	HAL_Delay(15000);
#endif // ST7735_IS_128X128
    }
}

/// Timer 3 is a 16 bit timer.
#define TICK_FREQ 96000000
#define SCALER 8
#define PWM_TICK_FREQ (TICK_FREQ / SCALER)

/// Flag to turn on and off the beeper (set in the
/// USB thread because that has a tight loop)
bool beep = false;

/// Task 4: blink the green LED on and off for every full SDRAM write/read
void sounder() {

    // Frequencies
    uint32_t arpeggio[] = {
	PWM_TICK_FREQ / 440, // A
	PWM_TICK_FREQ / 554, // C sharp
	PWM_TICK_FREQ / 659, // E
	PWM_TICK_FREQ / 880, // A
	PWM_TICK_FREQ / 659, // E
	PWM_TICK_FREQ / 554, // C sharp
	PWM_TICK_FREQ / 440, // A
    };
    
    while(1) {
	if (beep) {
	    for (unsigned n = 0; n < 7; ++n) {

		uint32_t pwm_period = arpeggio[n];
		uint32_t pwm_duty = pwm_period / 2;

		TIM3->CNT = 0;
		TIM3->ARR = pwm_period;
		TIM3->CCR2 = pwm_duty;
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
		osDelay(50);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
	    }
	}
        osDelay(1000);
    }
}

// This is defined in usb_host.c, and is modified by the autogenerated
// USBH_UserProcess1 and USBH_UserProcess2 functions, which are called when
// the state changes (i.e. device attached/disconnected, or configured).
extern ApplicationTypeDef USB1_APP_STATE;
extern ApplicationTypeDef USB2_APP_STATE;

/// Listen for attachments (full enumerations) on the two USB ports
/// and turn on the corresponding LED 
void usb_enumerate_listener() {
    
    while (1) {

	// This is fairly strong -- it means enumeration succeeded,
	// and the device class was detected and enabled.
	bool usb1_ready = (USB1_APP_STATE == APPLICATION_READY);
	bool usb2_ready = (USB2_APP_STATE == APPLICATION_READY);
	
	if (usb1_ready || usb2_ready) {
	    HAL_GPIO_WritePin(LD7_GPIO_Port, LD7_Pin, 1);
	}
	else {
	    HAL_GPIO_WritePin(LD7_GPIO_Port, LD7_Pin, 0);
	}

	// Check whether to turn on or off the SDRAM test or beep
	// Need to add debouncing here for reliability
	if (HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin) == 1) {
	    do_sdram_test = !do_sdram_test;
	}
	if (HAL_GPIO_ReadPin(BTN3_GPIO_Port, BTN3_Pin) == 1) {
	    beep = !beep;
	}


	
        osDelay(10);
    }
}


void user_main() {

    // Configure and start task 1 (blinking LED)
    const osThreadAttr_t sdram_test_attr = {
	.name = "sdram_test",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };
    osThreadNew(sdram_test, NULL, &sdram_test_attr);

    // Configure and start task 2 (neopixel colour cycling)
    const osThreadAttr_t neopixel_colours_attr = {
	.name = "neopixel_colours",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };    
    osThreadNew(neopixel_colours, NULL, &neopixel_colours_attr);

    // Configure and start task 3 (TFT display test)
    const osThreadAttr_t display_test_attr = {
	.name = "display_test",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };    
    osThreadNew(display_test, NULL, &display_test_attr);

    // Configure and start task 4 (sounder test)
    const osThreadAttr_t sounder_attr = {
	.name = "sounder",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };    
    osThreadNew(sounder, NULL, &sounder_attr);

    // Configure and start listening to USB ports
    const osThreadAttr_t usb_enumerate_listener_attr = {
	.name = "usb_enumerate_listener",
	.stack_size = 128 * 4,
	.priority = (osPriority_t) osPriorityNormal,
    };
    osThreadNew(usb_enumerate_listener, NULL, &usb_enumerate_listener_attr);
    
    
    osDelay(osWaitForever);
    while (1)
	;
}
