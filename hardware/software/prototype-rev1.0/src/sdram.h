#include <stdlib.h>
#include "stm32f7xx_hal.h"

// Bank 1 (of the MCU FMC) is mapped to 0xC0000000
#define SDRAM_DEVICE_ADDR ((uint32_t)0xC0000000)

// IS42S16800F-7TLI is 128Mbit (16 MiB)
#define SDRAM_DEVICE_SIZE ((uint32_t)16*1024*1024)

// The datasheet for IS42S16800F states (for -7TLI part, industrial) that
// there are 4096 refresh cycles in 64ms, so one every 15.56us.
// Round down to 15us for tolerance. The MCU clock frequency is 192MHz,
// so refresh counter = 15us x 192MHz = 2880
#define REFRESH_COUNT ((uint32_t)2880) // SDRAM refresh counter

// Set a long timeout
#define SDRAM_TIMEOUT ((uint32_t)0xFFFF)

// SDRAM mode register
#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)
 
void sdram_init_sequence(uint32_t refresh_count);
