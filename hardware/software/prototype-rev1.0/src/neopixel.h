#ifndef NEOPIXEL_H
#define NEOPIXEL_H

#include <stdint.h>
#include <stdlib.h>

// Number of pixels in the strip
#define NUM_PIXELS 38

// Number of bytes per pixel (24-bit colour, RGB)
#define BYTES_PER_PIXEL 3

// A mask that will be AND-ed with the LED uint8_t RGB
// values to limit them to (e.g.) 31. This is bright enough
// (nearly too bright), and provides some protection against
// overcurrent conditions on the development board.
#define COLOUR_MASK 0x1f

// Counter ticks required to generate a PWM period of 800kHz. This
// value assumes an APB1 clock frequency of 96MHz.
#define PWM_PERIOD 120

// Each bit is transmitted in a single PWM high-to-low pulse. The
// high-time determines whether a 1 or 0 is transmitted (64% for one,
// 32% for zero).
#define ONE_VALUE 0.64*PWM_PERIOD
#define ZERO_VALUE 0.32*PWM_PERIOD

// This is the length of the RGB buffer for the neopixels.
#define BUFFER_LEN NUM_PIXELS*BYTES_PER_PIXEL*8

/**
 * \brief Flush the neopixel bit buffer to the neopixel LEDs
 *
 * This is a non-blocking DMA operation.
 */
void write_neopixels(void);

/**
 * \brief Set all the neopixel states to off in the buffer
 *
 * Does not affect the state of the LEDs.
 */
void clear_buffer(void);

/**
 * \brief Set the colour of one neopixel
 *
 * Set the RGB colour of a neopixel. Colour values (ordinarily
 * up to 255) are masked using COLOUR_MASK to reduce the chance
 * of board overcurrent conditions. Note that this will cause
 * odd-looking behaviour (wrapping intensities that are out-of-
 * range), but is cheaper as a simple protection than normalising
 * the intensity.
 *
 * \return 0 on success, -1 for n out-of-range.
 */
int set_neopixel(size_t n, uint8_t r, uint8_t g, uint8_t b);

#endif
