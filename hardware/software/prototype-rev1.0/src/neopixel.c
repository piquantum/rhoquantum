#include "neopixel.h"
#include "main.h"

extern TIM_HandleTypeDef htim4;

// Each element of this array is a bit to be sent to the Neopixels
//
// Neopixels require their RGB values transmitted as a stream of bytes
// over the single data line. Each neopixel takes the first 24-bit
// colour code it receives as its own colour value, and passes
// subsequently received data onto the next neopixel. Therefore, low
// addresses in the buffer correspond to neopixels near the start of
// the chain, and and high addresses are the neopixels at the end of
// the wire.
//
// Each neopixel requires 3 bytes (it is a 24-bit colour code,
// arranged as GRB). Bits most be sent big-endian to the neopixels
// (so most-significant bit first).
//
// Each bit is encoded in this array as ONE_VALUE or ZERO_VALUE, to
// set the correct pulse high time that represents the bit value.
//
uint32_t bit_buffer[BUFFER_LEN+1];

/// Write the bit_buffer buffer to the neopixels
void write_neopixels(void) {

    // Neopixels seem to flicker without DMA stop-before-start
    // Probably time to read the manual.
    HAL_TIM_PWM_Stop_DMA(&htim4, TIM_CHANNEL_1);

    // Start the DMA transfer
    HAL_TIM_PWM_Start_DMA(&htim4, TIM_CHANNEL_1, bit_buffer, BUFFER_LEN+1);
}

/// Set the bit_buffer buffer to all neopixels off
void clear_buffer(void) {
    for (int n=0; n < BUFFER_LEN; n++) {
	bit_buffer[n] = ZERO_VALUE;
    }
    bit_buffer[BUFFER_LEN] = 0;
}

/// Write each bit of value to elements of the bit_buffer array, starting
/// from base (encoded using ZERO_VALUE and ONE_VALUE)
void write_byte(size_t base, uint8_t value) {

    // Limit brightness to protect board from overcurrent
    //value &= COLOUR_MASK;
    
    // Note the bytes are stored in big-endian format
    // (more significant bits at lower addresses).
    for (int n=0; n < 8; n++) {
	if (1 & (value >> n)) {
	    bit_buffer[base+7-n] = ONE_VALUE;
	} else {
	    bit_buffer[base+7-n] = ZERO_VALUE;
	}
    }
}

/// Set one pixel BIT_BUFFER value. Returns 0 on success,
/// and -1 for n out of range.
int set_neopixel(size_t n, uint8_t r, uint8_t g, uint8_t b) {
    
    if (n >= NUM_PIXELS) {
	return -1;
    }

    // Note that the RGB order is GRB.
    int base = BYTES_PER_PIXEL*n*8;
    write_byte(base, g);
    write_byte(base + 8, r);
    write_byte(base + 16, b);

    return 0;
}
