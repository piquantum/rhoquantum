import pandas as pd
import numpy as np

bom = pd.read_csv("bom.csv")

# Get the relevant columns
parts = bom[["Qty", "Reference", "Value", "Farnell", "Mouser", "Amazon", "PiHut"]].copy()

# Adjust the Neopixel capacitor quantity to the correct value. There are
# 38 Neopixels, so 38 100nF capacitors are required. Of these,
# 2 x 9 = 18 are in the schematic, so an additional 38 - 18 = 20 are
# required
parts.loc[parts["Farnell"] == "885012207016", "Qty"] += 20

# Add an entry for the potentiometer shafts
parts.loc[len(parts)] = ["2", "SHx", "Pot. Shafts", "JPEPL5012NE", "JPEPL5012NE", np.nan, np.nan]

# Add an entry for the FFC cables required (one spare)
parts.loc[len(parts)] = ["5", "FFCx", "FFC Cables", "MP-FFCA10060503A", "10-06-A-0050-C-4-08-4-T", np.nan, np.nan]

# Get the Farnell parts list
farnell_parts = parts[["Qty", "Reference", "Value", "Farnell"]].copy().dropna().set_index("Farnell")

# Get the Mouser parts list
mouser_parts = parts[["Qty", "Reference", "Value", "Mouser"]].copy().dropna().set_index("Mouser")

# Get the PiHut parts
pihut_parts = parts[["Qty", "Reference", "Value", "PiHut"]].copy().dropna().set_index("PiHut")

# Adjust the Neopixel quantity to the correct value (each bag of Neopixels
# contains 10, and 2 x 19 = 38 LEDs are required)
pihut_parts.loc["sku_ADA1655", "Qty"] = 4

# Get the Amazon parts
amazon_parts = bom[["Qty", "Reference", "Value", "Amazon"]].copy().dropna().set_index("Amazon")

# Insert the part number for the Amazon plastic screw/spacer kit
amazon_parts.loc["asin_B08HLKSKDQ"] = ["1", "SSx", "Screws/spacers"]


# Export separate lists for Farnell, Pihut, and Amazon
farnell_parts.to_csv("farnell_bom.csv")
mouser_parts.to_csv("mouser_bom.csv")
amazon_parts.to_csv("amazon_bom.csv")
pihut_parts.to_csv("pihut_bom.csv")

print("\nFarnell Parts:")
print(farnell_parts)
print("\nMouser Parts:")
print(mouser_parts)
print("\nAmazon Parts:")
print(amazon_parts)
print("\nPiHut Parts:")
print(pihut_parts)

