import subprocess
from itertools import combinations

def make_command_list(args: dict[str, str | None]) -> list[str]:
    """Make the command suitable for use with subprocess.run()

    Args:
        args: A dictionary of arguments that will be appended to
            the default included in this function.

    Returns:
        A list containing the command name, the arguments encoded
            as a list, and the PCB file name.

    """
    command = "generate_interactive_bom"

    base_args = {
        #"--dark-mode": None,
        "--highlight-pin1": "selected",
        "--dest-dir": "board_view",
        "--no-browser": None,
        "--extra-data-file": "rhoquantum.net",
        "--variant-field": "Board",
    }

    command_list = [command]
    all_args = base_args | args
    for arg_name, value in all_args.items():
        command_list.append(arg_name)
        if value is not None:
            command_list.append(value)

    # Append the final argument name (the PCB file)
    command_list.append("rhoquantum.kicad_pcb")
    return command_list


def get_board_strings(board_name: str) -> str:
    """Get the valid board name strings for board_name
    """

    # The board variants will be listed in this order in
    # the "Board" field of the schematic, separated by a
    # semicolon.
    board_names = ["BL", "TL", "TR", "BR", "Test"]
    
    # Board order is not lexicographic, so use
    # indices instead. The aim is to get a list
    # of all subsets of the boards, preserving
    # the order.
    name_indices = range(5)
    all_index_combos = sum([list(combinations(name_indices, r)) for r in range(1,6)], [])
    all_boards = [[board_names[i] for i in indices] for indices in all_index_combos]

    all_boards_with_board_name = [x for x in all_boards if board_name in x]
    board_strings = [";".join(x) for x in all_boards_with_board_name]

    board_strings_comma_separated = ",".join(board_strings)
    
    return board_strings_comma_separated

def make_board_view(board_name: str, group_by: list[str], file_name_suffix: str, rotate: int) -> None:

    file_names = {
        "BL": "bottom_left",
        "TL": "top_left",
        "TR": "top_right",
        "BR": "bottom_right"
    }
    
    args = {
        "--checkboxes": "Placed",
        "--show-fields": "Value,Part Number,Assembly Group",
        "--group-fields": ",".join(group_by),
        "--board-rotation": str(rotate),
        "--variants-whitelist": get_board_strings(board_name),
        "--name-format": f"{file_names[board_name]}_{file_name_suffix}",
    }
    
    command_list = make_command_list(args)

    subprocess.run(command_list)

def make_test_points() -> None:
    
    args = {
        "--checkboxes": "",
        "--show-fields": "Value,Description",
        "--variants-whitelist": get_board_strings("Test"),
        "--name-format": f"test_points",
    }

    command_list = make_command_list(args)

    subprocess.run(command_list)
    
# Make the summary board views. These group together the
# assembly groups for a quick overview of where everything
# is located.
make_board_view("BL", ["Assembly Group"], "summary", 0)
make_board_view("TL", ["Assembly Group"], "summary", 180)
make_board_view("TR", ["Assembly Group"], "summary", 180)
make_board_view("BR", ["Assembly Group"], "summary", 0)

# Make the assembly board views. These list each component
# on a separate line, which is more useful for knowing in
# detail which component is which
make_board_view("BL", ["Value", "Assembly Group"], "assembly", 0)
make_board_view("TL", ["Value", "Assembly Group"], "assembly", 180)
make_board_view("TR", ["Value", "Assembly Group"], "assembly", 180)
make_board_view("BR", ["Value", "Assembly Group"], "assembly", 0)

# Make a file for test point data
make_test_points()
