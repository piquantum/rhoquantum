import pathlib

docs_dir = pathlib.Path("docs_board_view")
docs_dir.mkdir(parents=True, exist_ok=True)

# Process HTML files for Antora site
for html_file in pathlib.Path("board_view").glob("*.html"):

    s = html_file.read_text()
    # Escape ampersand and double-quote so that the
    # file can be inserted into the srcdoc attribute of
    # iframe as a string (for antora docs)
    s = s.replace('&', "&amp;")
    s = s.replace('"', "&quot;")
        
    new_file = (docs_dir / html_file.name).write_text(s)
    
