/*
 * gpio.c
 *
 *  Created on: Jun 1, 2024
 *      Author: jrs
 */

#include <main.h>
#include "gpio.h"
#include "cmsis_os2.h"

void set_led(unsigned which, unsigned state) {
	switch (which) {
	case LED0:
		HAL_GPIO_WritePin(LD7_GPIO_Port, LD7_Pin, state);
		break;
	case LED1:
		HAL_GPIO_WritePin(LD8_GPIO_Port, LD8_Pin, state);
		break;
	default:
		// Handle error -- unknown LED
	}
}

void toggle_led(unsigned which) {
	switch (which) {
	case LED0:
		HAL_GPIO_TogglePin(LD7_GPIO_Port, LD7_Pin);
		break;
	case LED1:
		HAL_GPIO_TogglePin(LD8_GPIO_Port, LD8_Pin);
		break;
	default:
		// Handle error -- unknown LED
	}
}

unsigned get_button(unsigned which) {
	switch (which) {
	case BTN0:
		return HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin);
	case BTN1:
		return HAL_GPIO_ReadPin(BTN3_GPIO_Port, BTN3_Pin);
	default:
		// Handle error -- unknown button
	}
}

void block_until_button_state(unsigned which, unsigned state) {
	unsigned count = 0;
	while (++count < DEBOUNCE_MILLISECONDS) {
		if (get_button(which) != state) {
			count = 0;
		}
		osDelay(1); // 1ms
	}
}
