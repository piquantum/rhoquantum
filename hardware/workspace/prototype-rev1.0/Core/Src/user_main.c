/*
 * user_main.c
 *
 *  Created on: Jun 5, 2024
 *      Author: jrs
 */

#include <stdio.h>
#include "gpio.h"
#include "sdram.h"
#include "mem_test.h"
#include <stdlib.h>
#include "neopixel.h"
#include "cmsis_os2.h"
#include "st7735.h"
#include "audio.h"
#include "usb.h"

extern UART_HandleTypeDef huart5;

void user_main() {

	// Configure and start listening to USB ports
	const osThreadAttr_t usb_enumerate_listener_attr = { .name =
			"usb_enumerate_listener", .stack_size = 128 * 4, .priority =
			(osPriority_t) osPriorityNormal, };
	osThreadNew(usb_enumerate_listener, NULL, &usb_enumerate_listener_attr);

	// Configure and start the audio test
	const osThreadAttr_t audio_test_attr = { .name = "audio_test", .stack_size =
			128 * 4, .priority = (osPriority_t) osPriorityNormal, };
	osThreadNew(audio_test, NULL, &audio_test_attr);

	// Configure and start the display test
	// Configure and start task 3 (TFT display test)
	const osThreadAttr_t display_test_attr =
			{ .name = "display_test", .stack_size = 128 * 4, .priority =
					(osPriority_t) osPriorityNormal, };
	osThreadNew(display_test, NULL, &display_test_attr);

	// Configure and start the Neopixel test
	const osThreadAttr_t neopixel_test_attr =
			{ .name = "neopixel_test", .stack_size = 128 * 4, .priority =
					(osPriority_t) osPriorityNormal, };
	osThreadNew(neopixel_test, NULL, &neopixel_test_attr);

	// Allocate the entire SDRAM
	unsigned char *buf = (unsigned char*) malloc(SDRAM_DEVICE_SIZE);
	if (buf != NULL) {
		// Perform a memory test
		printf("Starting first memory test on address %p\n", buf);
		if (mem_test(buf, SDRAM_DEVICE_SIZE) == 0) {
			printf(" - Memory test passed\n");
		} else {
			printf(" - Memory test failed\n");
		}
	} else {
		printf("Malloc failed!\n");
	}

	uint8_t uart_data;
	while (1) {
		HAL_UART_Receive(&huart5, &uart_data, 1, 0xffffffff);
		HAL_UART_Transmit(&huart5, &uart_data, 1, 0xffffffff);
	}

	// Place an infinite loop at the end of main
	// to ensure no return
	while (1)
		;
}
