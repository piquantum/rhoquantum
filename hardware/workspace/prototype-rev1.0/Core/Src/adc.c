/*
 * adc.c
 *
 *  Created on: Jun 5, 2024
 *      Author: john
 */

#include "stm32f7xx.h"

extern ADC_HandleTypeDef hadc1;

/**
 * \brief Returns a value between 0.0 and 1.0 for the ADC reading
 *
 * Assuming 12-bit ADC, set a range of voltages that will be mapped
 * to [0.0, 1.0]
 *
 *
 */
float read_potentiometer(float low_voltage, float high_voltage) {

    HAL_ADC_Start(&hadc1);
    while (HAL_ADC_PollForConversion(&hadc1, 1000) != HAL_OK)
        ; // Wait for ADC to complete

    uint32_t adc_value = HAL_ADC_GetValue(&hadc1);
    HAL_ADC_Stop(&hadc1);

    // VREF+ is 3.3V and VREF- is 0V, so the 12 bits equally
    // divide the range 0-3.3V
    const uint32_t adc_3v3 = 0xfff;

    float voltage = 3.3 * (float)adc_value / (float)adc_3v3;

    if (voltage > high_voltage) {
        return 1.0;
    } else if (voltage < low_voltage) {
        return 0.0;
    } else {
        // Voltage is in range
        return (voltage - low_voltage) / (high_voltage - low_voltage);
    }
}
