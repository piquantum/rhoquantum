/*
 * audio.c
 *
 *  Created on: Jun 5, 2024
 *      Author: john
 */

#include <stdbool.h>
#include "cmsis_os2.h"

#include "stm32f7xx.h"
#include "audio.h"

extern TIM_HandleTypeDef htim3;

void audio_test() {

	// Frequencies
	uint32_t arpeggio[] = {
	PWM_TICK_FREQ / 440, // A
	PWM_TICK_FREQ / 554, // C sharp
	PWM_TICK_FREQ / 659, // E
	PWM_TICK_FREQ / 880, // A
	PWM_TICK_FREQ / 659, // E
	PWM_TICK_FREQ / 554, // C sharp
	PWM_TICK_FREQ / 440, // A
			};

	while (1) {

		for (unsigned n = 0; n < 7; ++n) {

			uint32_t pwm_period = arpeggio[n];
			uint32_t pwm_duty = pwm_period / 2;

			TIM3->CNT = 0;
			TIM3->ARR = pwm_period;
			TIM3->CCR2 = pwm_duty;
			HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
			osDelay(50);
			HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
		}

		osDelay(1000);
	}
}
