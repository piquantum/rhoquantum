/*
 * audio.h
 *
 *  Created on: Jun 5, 2024
 *      Author: john
 */

#ifndef INC_AUDIO_H_
#define INC_AUDIO_H_

#include <stdbool.h>
#include "cmsis_os2.h"

/// Timer 3 is a 16 bit timer, scale to keep ticks in range
#define TICK_FREQ 96000000
#define SCALER 75
#define PWM_TICK_FREQ (TICK_FREQ / SCALER)

// Test the audio
void audio_test();


#endif /* INC_AUDIO_H_ */
