/*
 * user_main.h
 *
 *  Created on: Jun 5, 2024
 *      Author: jrs
 */

#ifndef INC_USER_MAIN_H_
#define INC_USER_MAIN_H_

// Function called from the default task in main.c, used
// to separate user application code into a separate file
// for ease of programming
void user_main();

#endif /* INC_USER_MAIN_H_ */
