/*
 * adc.h
 *
 *  Created on: Jun 5, 2024
 *      Author: john
 */

#ifndef INC_ADC_H_
#define INC_ADC_H_

/**
 * \brief Returns a value between 0.0 and 1.0 for the ADC reading
 *
 * Assuming 12-bit ADC, set a range of voltages that will be mapped
 * to [0.0, 1.0]
 *
 */
float read_potentiometer(float low_voltage, float high_voltage);

#endif /* INC_ADC_H_ */
