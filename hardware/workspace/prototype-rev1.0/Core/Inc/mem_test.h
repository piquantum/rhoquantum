/*
 * mem_test.h
 *
 *  Created on: Jun 5, 2024
 *      Author: jrs
 */

#ifndef INC_MEM_TEST_H_
#define INC_MEM_TEST_H_

#include <stddef.h>

// Set the data bus width for the memory test
typedef unsigned char datum;

// Perform a full memory test. Specify the base address of the
// memory device, and the length of the device in bytes.
int mem_test(volatile datum *base_address, size_t num_bytes);

#endif /* INC_MEM_TEST_H_ */
