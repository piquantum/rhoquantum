/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LD7_Pin GPIO_PIN_4
#define LD7_GPIO_Port GPIOA
#define LD8_Pin GPIO_PIN_5
#define LD8_GPIO_Port GPIOA
#define BTN2_Pin GPIO_PIN_13
#define BTN2_GPIO_Port GPIOD
#define BTN3_Pin GPIO_PIN_3
#define BTN3_GPIO_Port GPIOG
#define AUDIO_PWM_Pin GPIO_PIN_7
#define AUDIO_PWM_GPIO_Port GPIOC
#define USB_EN_B_Pin GPIO_PIN_9
#define USB_EN_B_GPIO_Port GPIOC
#define USB_FLAG_B_Pin GPIO_PIN_8
#define USB_FLAG_B_GPIO_Port GPIOA
#define USB_FLAG_A_Pin GPIO_PIN_9
#define USB_FLAG_A_GPIO_Port GPIOA
#define USB_EN_A_Pin GPIO_PIN_10
#define USB_EN_A_GPIO_Port GPIOA
#define ST7735_RS_Pin GPIO_PIN_15
#define ST7735_RS_GPIO_Port GPIOA
#define ST7735_RST_Pin GPIO_PIN_10
#define ST7735_RST_GPIO_Port GPIOC
#define ST7735_CS_Pin GPIO_PIN_11
#define ST7735_CS_GPIO_Port GPIOC
#define LED_DIN_Pin GPIO_PIN_6
#define LED_DIN_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
