/*
 * gpio.h
 *
 *  Created on: Jun 1, 2024
 *      Author: jrs
 */

#ifndef INC_GPIO_H_
#define INC_GPIO_H_

// To make the code portable, use generic names
// for the LEDs and buttons. Use these variables
// as the which argument in the functions below.
#define LED0 0
#define LED1 1
#define BTN0 0
#define BTN1 1

void set_led(unsigned which, unsigned state);
void toggle_led(unsigned which);
unsigned get_button(unsigned which);

// Set required delay before button change
// is accepted. Button state sampled once
// per millisecond must remain the same
// for this period.
#define DEBOUNCE_MILLISECONDS 10

// Debounced wait for button state
void block_until_button_state(unsigned which, unsigned state);

#endif /* INC_GPIO_H_ */
